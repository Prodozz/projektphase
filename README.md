car:
```
{
"brand" : "BMW",
"modelName" : "MODEL_ONE",
"mileage" : 245524.10,
"fuelType" : "DIESEL",
"finNumber" : "qrtag",
"whenBought" : "10-10-2010",
"buildDate" : "01-01-2001",
"kw" : 10,
"contractTerm" : "20 Months",
"monthlyRate" : 2000.10,
"carProperties" : ["nice", "good"],
"contractEntities" : [],
"ratingEntities" :[]
}
```

customer:
```
{
    "username": "Mansur",
    "password": "12345",
    "phoneNumber": "+23452345",
    "birthDate": "11-03-2001",
    "firstName": "Mansur",
    "lastName": "Bib"
}
```
employee:
```
{
    "username": "adsaga",
    "password": "12345",
    "phoneNumber": "+23452345",
    "birthDate": "11-03-2001",
    "firstName": "Mansur",
    "lastName": "Bib",
    "grossSalary": 2334.10,
    "employmentType": "FULLTIME",
    "position": "WORKER"
    
}
```
address
```
{
    "state" : "adsf",
    "district" : "dffggf",
    "streetName" : "dgfpjgfdap",
    "streetNumber" : 3,
    "houseNumber" : 4,
    "door" : 2,
    "customerId": null,
    "employeeId" : null 
}
```
contract_paymentstatus:
```
{
    "intervalOfPayments" : "QUARTERLY",
    "intervalRate" : "420 EUro",
    "contractId" : null
}
```
contract:
```
{
    "rentStartDate" : "01-01-2001",
    "rentEndDate" : "02-01-2001",
    "contractDate" : "10-10-2010",
    "monthlyRate" : 20004.10,
    "contractTerm": "12 month",
    "isActive" : true,
    "rating" : 5,
    "feedbackText" : "aöfdojafdsojfdasojöfadsj",
    "carId" : null,
    "customerId" : null,
    "employeeId" : null
}
```
motorcaraccident:
```
{
    "accidentHeader" : "afkdjlbadsfkljn",
    "accidentDescriptionText" : "dgaoijgfdjjoidgf",
    "customerId" : null,
    "employeeId" : null
}
```
rating:
```
[
    {
        "ratingId": "052d1823-9119-4466-b592-d8b51d0c3980",
        "rating": 5,
        "feedbackText": "dfjfadsljk",
        "customerId": null,
        "carId": null
    }
]
```




