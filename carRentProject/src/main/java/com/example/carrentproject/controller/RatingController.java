package com.example.carrentproject.controller;

import com.example.carrentproject.DTOs.RatingDTOs.RatingDTO;
import com.example.carrentproject.repository.RatingCRUDRepository;
import com.example.carrentproject.service.RatingService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequestMapping(path = "/api/rating")
@AllArgsConstructor
public class RatingController {

    final RatingService ratingService;
    final RatingCRUDRepository ratingCRUDRepository;

    @GetMapping
    public ResponseEntity<?> getAllRatings() {
        try {
            return ResponseEntity.ok(ratingService.getAllRatings());
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/{ratingId}")
    public ResponseEntity<?> getRating(@PathVariable String ratingId) {
        try {
            return ResponseEntity.ok(ratingService.getRating(ratingId));
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping
    public ResponseEntity<?> createRating(@RequestBody RatingDTO ratingDTO) {
        try {
            ratingService.createRating(ratingDTO);
            return ResponseEntity.ok(ratingService.getAllRatings());
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }
    @PutMapping
    public ResponseEntity<?> updateRating(@RequestBody RatingDTO ratingDTO) {
        try {
            ratingService.updateRating(ratingDTO);
            return ResponseEntity.ok(ratingService.getAllRatings());
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @DeleteMapping("/{ratingId}")
    public ResponseEntity<?> deleteRating(@PathVariable String ratingId) {
        try {
            ratingService.deleteRating(ratingId);
            return ResponseEntity.ok(ratingService.getAllRatings());
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }


}
















