package com.example.carrentproject.controller;

import com.example.carrentproject.DTOs.motorcarAccidentDTOs.MotorcarAccidentDTO;
import com.example.carrentproject.service.MotorcarAccidentService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequestMapping(path = "/api/motorcaraccident")
@AllArgsConstructor
public class MotorcarAccidentController {

    final MotorcarAccidentService motorcarAccidentService;

    @GetMapping
    public ResponseEntity<?> getAllMotorcarAccidents() {
        try {
            return ResponseEntity.ok(motorcarAccidentService.getAllMotorcarAccidents());
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/{motorcaraccidentId}")
    public ResponseEntity<?> getMotorcarAccident(@PathVariable String motorcaraccidentId) {
        try {
            return ResponseEntity.ok(motorcarAccidentService.getMotorcarAccident(motorcaraccidentId));
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping
    public ResponseEntity<?> createMotorcarAccident(@RequestBody MotorcarAccidentDTO motorcarAccidentDTO) {
        try {
            motorcarAccidentService.createMotorcarAccident(motorcarAccidentDTO);
            return ResponseEntity.ok(motorcarAccidentService.getAllMotorcarAccidents());
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }
    @PutMapping
    public ResponseEntity<?> updateMotorcarAccident(@RequestBody MotorcarAccidentDTO motorcarAccidentDTO) {
        try {
            motorcarAccidentService.updateMotorcarAccident(motorcarAccidentDTO);
            return ResponseEntity.ok(motorcarAccidentService.getAllMotorcarAccidents());
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @DeleteMapping("/{motorcaraccidentId}")
    public ResponseEntity<?> deleteMotorcarAccident(@PathVariable String motorcaraccidentId) {
        try {
            motorcarAccidentService.deleteMotorcarAccident(motorcaraccidentId);
            return ResponseEntity.ok(motorcarAccidentService.getAllMotorcarAccidents());
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }


}
















