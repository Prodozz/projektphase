package com.example.carrentproject.controller;

import com.example.carrentproject.DTOs.adressDTOs.AddressDTO;
import com.example.carrentproject.repository.AddressCRUDRepository;
import com.example.carrentproject.service.AddressService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequestMapping(path = "/api/address")
@AllArgsConstructor
public class AddressController {

    final AddressService addressService;
    final AddressCRUDRepository addressCRUDRepository;

    @GetMapping
    public ResponseEntity<?> getAllAddresses() {
        try {
            return ResponseEntity.ok(addressService.getAllAddresses());
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/{addressId}")
    public ResponseEntity<?> getAddress(@PathVariable String addressId) {
        try {
            return ResponseEntity.ok(addressService.getAddress(addressId));
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping
    public ResponseEntity<?> createAddress(@RequestBody AddressDTO addressDTO) {
        try {
            addressService.createAddress(addressDTO);
            return ResponseEntity.ok(addressService.getAllAddresses());
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }
    @PutMapping
    public ResponseEntity<?> updateAddress(@RequestBody AddressDTO addressDTO) {
        try {
            addressService.updateAddress(addressDTO);
            return ResponseEntity.ok(addressService.getAllAddresses());
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @DeleteMapping("/{addressId}")
    public ResponseEntity<?> deleteAddress(@PathVariable String addressId) {
        try {
            addressService.deleteAddress(addressId);
            return ResponseEntity.ok(addressService.getAllAddresses());
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }


}
















