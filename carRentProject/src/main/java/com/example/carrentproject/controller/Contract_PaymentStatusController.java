package com.example.carrentproject.controller;

import com.example.carrentproject.DTOs.contractPaymentStatusDTOs.Contract_PaymentStatusDTO;
import com.example.carrentproject.repository.Contract_PaymentStatusCRUDRepository;
import com.example.carrentproject.service.Contract_PaymentStatusService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequestMapping(path = "/api/contract_paymentstatus")
@AllArgsConstructor
public class Contract_PaymentStatusController {

    final Contract_PaymentStatusService contract_PaymentStatusService;
    final Contract_PaymentStatusCRUDRepository contract_PaymentStatusCRUDRepository;

    @GetMapping
    public ResponseEntity<?> getAllContract_PaymentStatus() {
        try {
            return ResponseEntity.ok(contract_PaymentStatusService.getAllContract_PaymentStatus());
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/{contract_PaymentStatusId}")
    public ResponseEntity<?> getContract_PaymentStatus(@PathVariable String contract_PaymentStatusId) {
        try {
            return ResponseEntity.ok(contract_PaymentStatusService.getContract_PaymentStatus(contract_PaymentStatusId));
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping
    public ResponseEntity<?> createContract_PaymentStatus(@RequestBody Contract_PaymentStatusDTO contract_PaymentStatusDTO) {
        try {
            contract_PaymentStatusService.createContract_PaymentStatus(contract_PaymentStatusDTO);
            return ResponseEntity.ok(contract_PaymentStatusService.getAllContract_PaymentStatus());
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }
    @PutMapping
    public ResponseEntity<?> updateContract_PaymentStatus(@RequestBody Contract_PaymentStatusDTO contract_PaymentStatusDTO) {
        try {
            contract_PaymentStatusService.updateContract_PaymentStatus(contract_PaymentStatusDTO);
            return ResponseEntity.ok(contract_PaymentStatusService.getAllContract_PaymentStatus());
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @DeleteMapping("/{contract_PaymentStatusId}")
    public ResponseEntity<?> deleteContract_PaymentStatus(@PathVariable String contract_PaymentStatusId) {
        try {
            contract_PaymentStatusService.deleteContract_PaymentStatus(contract_PaymentStatusId);
            return ResponseEntity.ok(contract_PaymentStatusService.getAllContract_PaymentStatus());
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }


}
















