package com.example.carrentproject.controller;

import com.example.carrentproject.DTOs.contractDTOs.ContractDTO;
import com.example.carrentproject.repository.ContractCRUDRepository;
import com.example.carrentproject.service.ContractService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequestMapping(path = "/api/contract")
@AllArgsConstructor
public class ContractController {

    final ContractService contractService;
    final ContractCRUDRepository contractCRUDRepository;

    @GetMapping
    public ResponseEntity<?> getAllContracts() {
        try {
            return ResponseEntity.ok(contractService.getAllContracts());
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/{contractId}")
    public ResponseEntity<?> getContract(@PathVariable String contractId) {
        try {
            return ResponseEntity.ok(contractService.getContract(contractId));
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping
    public ResponseEntity<?> createContract(@RequestBody ContractDTO contractDTO) {
        try {
            contractService.createContract(contractDTO);
            return ResponseEntity.ok(contractService.getAllContracts());
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }
    @PutMapping
    public ResponseEntity<?> updateContract(@RequestBody ContractDTO contractDTO) {
        try {
            contractService.updateContract(contractDTO);
            return ResponseEntity.ok(contractService.getAllContracts());
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @DeleteMapping("/{contractId}")
    public ResponseEntity<?> deleteContract(@PathVariable String contractId) {
        try {
            contractService.deleteContract(contractId);
            return ResponseEntity.ok(contractService.getAllContracts());
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }


}
















