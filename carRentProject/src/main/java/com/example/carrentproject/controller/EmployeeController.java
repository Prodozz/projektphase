package com.example.carrentproject.controller;

import com.example.carrentproject.DTOs.employeeDTOs.EmployeeUserPropertiesDTO;
import com.example.carrentproject.DTOs.employeeDTOs.RegisterEmployeeDTO;
import com.example.carrentproject.repository.EmployeeCRUDRepository;
import com.example.carrentproject.service.EmployeeService;
import com.example.carrentproject.service.EmployeeUserService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.HttpStatus.BAD_REQUEST;

@RestController
@CrossOrigin
@RequestMapping(path = "/api/employee")
@AllArgsConstructor
public class EmployeeController {

    final EmployeeService employeeService;
    final EmployeeUserService employeeUserService;
    final EmployeeCRUDRepository employeeCRUDRepository;

    @GetMapping
    public ResponseEntity<?> getAllEmployees() {
        try {
            return ResponseEntity.ok(employeeService.getAllEmployees());
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), BAD_REQUEST);
        }
    }

    @GetMapping("/{employeeId}")
    public ResponseEntity<?> getEmployee(@PathVariable String employeeId) {
        try {
            return ResponseEntity.ok(employeeService.getEmployee(employeeId));
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), BAD_REQUEST);
        }
    }

    @GetMapping("/credentials/{employeeId}")
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<?> getEmployeeProperties(@PathVariable String employeeId) {
        try {
            return ResponseEntity.ok(employeeService.getEmployeePropertiesById(employeeId));
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), BAD_REQUEST);
        }
    }

    @PostMapping("/register")
    public ResponseEntity<?> createEmployee(@RequestBody RegisterEmployeeDTO registerEmployeeDTO) {
        try {
            employeeUserService.createEmployeeUser(registerEmployeeDTO);
            return ResponseEntity.ok(employeeService.getEmployeePropertiesByDTO(registerEmployeeDTO));
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), BAD_REQUEST);
        }
    }

    @PutMapping
    public ResponseEntity<?> updateEmployeeProperties(@RequestBody EmployeeUserPropertiesDTO employeeUserPropertiesDTO) {
        try {
            employeeService.updateEmployeeProperties(employeeUserPropertiesDTO);
            return ResponseEntity.ok(employeeService.getEmployeePropertiesById(employeeUserPropertiesDTO.getEmployeeId()));
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), BAD_REQUEST);
        }
    }




}
















