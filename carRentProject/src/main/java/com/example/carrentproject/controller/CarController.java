package com.example.carrentproject.controller;

import com.example.carrentproject.DTOs.carDTOs.CarDTO;
import com.example.carrentproject.DTOs.carDTOs.CreateCarDTO;
import com.example.carrentproject.repository.CarCRUDRepository;
import com.example.carrentproject.service.CarService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequestMapping(path = "/api/car")
@AllArgsConstructor
public class CarController {

    final CarService carService;
    final CarCRUDRepository carCRUDRepository;

    @GetMapping
    public ResponseEntity<?> getAllCars() {
        try {
            return ResponseEntity.ok(carService.getAllCars());
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/{carId}")
    public ResponseEntity<?> getCar(@PathVariable String carId) {
        try {
            return ResponseEntity.ok(carService.getCar(carId));
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping
    public ResponseEntity<?> createCar(@RequestBody CreateCarDTO createCarDTO) {
        try {
            carService.createCar(createCarDTO);
            return ResponseEntity.ok(carService.getAllCars());
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }
    @PutMapping
    public ResponseEntity<?> updateCar(@RequestBody CarDTO carDTO) {
        try {
            carService.updateCar(carDTO);
            return ResponseEntity.ok(carService.getAllCars());
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @DeleteMapping("/{carId}")
    public ResponseEntity<?> deleteCar(@PathVariable String carId) {
        try {
            carService.deleteCar(carId);
            return ResponseEntity.ok(carService.getAllCars());
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }


}
















