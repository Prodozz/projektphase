package com.example.carrentproject.controller;

import com.example.carrentproject.DTOs.customerDTOs.CustomerUserPropertiesDTO;
import com.example.carrentproject.DTOs.customerDTOs.RegisterCustomerDTO;
import com.example.carrentproject.DTOs.customerDTOs.SetPaymentDTO;
import com.example.carrentproject.DTOs.userDTOs.UserLoginDTO;
import com.example.carrentproject.service.CustomerService;
import com.example.carrentproject.service.CustomerUserService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.HttpStatus.BAD_REQUEST;

@RestController
@CrossOrigin
@RequestMapping(path = "/api/customer")
@AllArgsConstructor
public class CustomerController {

    final CustomerService customerService;
    final UserDetailsManager userDetailsManager;
    final CustomerUserService customerUserService;

    @GetMapping
//    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<?> getAllCustomers() {
        try {
            return ResponseEntity.ok(customerService.getAllCustomers());
        } catch (Exception e) {
            return new ResponseEntity<>(e, BAD_REQUEST);
        }
    }

    @GetMapping("/{customerId}")
//    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<?> getCustomer(@PathVariable String customerId) {
        try {
            return ResponseEntity.ok(customerService.getCustomer(customerId));
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), BAD_REQUEST);
        }
    }

    @GetMapping("/credentials/{customerId}")
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<?> getCustomerPropertiesByCustomerId(@PathVariable String customerId) {
        try {
            return ResponseEntity.ok(customerService.getCustomerPropertiesById(customerId));
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), BAD_REQUEST);
        }
    }

    @PostMapping("/register")
    public ResponseEntity<?> createCustomer(@RequestBody RegisterCustomerDTO registerCustomerDTO) {
        try {
            customerUserService.createCustomerUser(registerCustomerDTO);
            return ResponseEntity.ok(customerService.getCustomerPropertiesByDTO(registerCustomerDTO));
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), BAD_REQUEST);
        }
    }
    @PutMapping("{customerId}")
//    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<?> updateCustomerProperties(@RequestBody CustomerUserPropertiesDTO customerUserPropertiesDTO) {
        try {
            customerService.updateCustomerProperties(customerUserPropertiesDTO);
            return ResponseEntity.ok(customerService.getCustomerPropertiesById(customerUserPropertiesDTO.getCustomerId()));
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), BAD_REQUEST);
        }
    }
    @PutMapping("/setPaymentMethod")
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<?> setCustomerPayment(@RequestBody SetPaymentDTO setPaymentDTO) {
        try {
            customerService.setPayment(setPaymentDTO);
            return ResponseEntity.ok(customerService.getCustomer(setPaymentDTO.getCustomerId()));
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), BAD_REQUEST);
        }
    }

    @PostMapping("/login")
    public ResponseEntity<?> loginCustomer (@RequestBody UserLoginDTO customerDTO) {
        try {
            return new ResponseEntity<>(customerService.loginCustomer(customerDTO), HttpStatus.OK);
        } catch (Exception e){
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }

    }

    // TODO: paymentstatus mit hasCreditWorthiness RestMethode entweder in CustomerController oder ContractPaymentStatus




}
















