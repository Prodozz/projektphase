package com.example.carrentproject.controller;

import com.example.carrentproject.enums.Status;
import org.apache.coyote.Response;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/api/enums")
public class EnumsController {

    @GetMapping("/status")
    public ResponseEntity getStatusEnum(){

        List<Status> status = Arrays.asList(Status.values());
        return new ResponseEntity(status, HttpStatus.OK);
    }
}
