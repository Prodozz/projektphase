package com.example.carrentproject.controller;

import com.example.carrentproject.DTOs.userDTOs.ChangePasswordDTO;
import com.example.carrentproject.DTOs.userDTOs.UserDetailsDTO;
import com.example.carrentproject.service.CustomerUserService;
import com.example.carrentproject.service.EmployeeUserService;
import com.example.carrentproject.service.UserEntityService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.HttpStatus.BAD_REQUEST;

@RestController
@CrossOrigin
@RequestMapping(path = "/api")
@AllArgsConstructor
public class UserController {


    private EmployeeUserService employeeUserService;
    private CustomerUserService customerUserService;
    private UserDetailsManager userDetailsManager;
    private UserEntityService userEntityService;



    @DeleteMapping("/employee/delete/{employeeId}")
    public ResponseEntity<?> deleteEmployee(@PathVariable String employeeId) {
        try {
            employeeUserService.deleteEmployeeUser(employeeId);
            return ResponseEntity.ok("Deleted!");
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), BAD_REQUEST);
        }
    }

        @DeleteMapping("/customer/delete/{customerId}")
    public ResponseEntity<?> deleteCustomer(@PathVariable String customerId) {
        try {
            customerUserService.deleteCustomerUser(customerId);
            return ResponseEntity.ok("Deleted!");
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), BAD_REQUEST);
        }
    }

    @PutMapping("/user/changePassword")
    public ResponseEntity<?> changePassword(@RequestBody ChangePasswordDTO changePasswordDTO) {
        try {
            userDetailsManager.changePassword(changePasswordDTO.getOldPassword(), changePasswordDTO.getNewPassword());
            return ResponseEntity.ok("");
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), BAD_REQUEST);
        }
    }

    @PutMapping("/user/update")
    public ResponseEntity<?> updateUser(@RequestBody UserDetailsDTO userDetailsDTO) {
        try {
            userDetailsManager.updateUser(userEntityService.createSecurityUser(userDetailsDTO));
            return ResponseEntity.ok("");
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), BAD_REQUEST);
        }
    }

}
