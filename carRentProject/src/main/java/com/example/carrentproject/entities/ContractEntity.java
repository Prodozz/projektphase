package com.example.carrentproject.entities;

import com.fasterxml.jackson.annotation.JsonFormat;
import jakarta.persistence.*;
import lombok.*;

import java.util.UUID;

@Entity
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Data
public class ContractEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    private UUID contractId;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private String rentStartDate;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private String rentEndDate;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private String contractDate;
    private Double monthlyRate;
    private String contractTerm;
    private Boolean isActive;
    private int rating;
    private String feedbackText;

    @ManyToOne
    @JoinColumn(name = "carId")
    private CarEntity car;

    @OneToOne(mappedBy = "contract")
    Contract_PaymentStatusEntity contractPaymentStatusEntity;

    @OneToOne
    @JoinColumn(name = "customerId")
    CustomerEntity customer;

    @OneToOne
    @JoinColumn(name = "employeeId")
    EmployeeEntity employee;



}
