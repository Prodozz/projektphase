package com.example.carrentproject.entities;

import com.example.carrentproject.enums.PaymentMethod;
import com.example.carrentproject.enums.Status;
import com.example.carrentproject.security.entities.SecurityUserEntity;
import jakarta.persistence.*;
import lombok.*;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.List;
import java.util.UUID;

@Entity
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CustomerEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    private UUID customerId;
    @Enumerated(EnumType.STRING)
    private PaymentMethod paymentMethod;
    @Enumerated(EnumType.STRING)
    private Status status;
    private Boolean hasCreditWorthiness;
    private String email;
    private String phoneNumber;
    private String birthDate;
    private String firstName;
    private String lastName;
    @Column(unique = true)
    private String username;

    @OneToOne
    @JoinColumn(name = "addressId")
    private AddressEntity address;

    @OneToOne(mappedBy = "customer")
    private MotorcarAccidentEntity motorcarAccidentEntity;
    @OneToOne(mappedBy = "customer")
    private ContractEntity contractEntity;
    @OneToMany(mappedBy = "customer")
    private List<RatingEntity> ratingEntities;


    public CustomerEntity(UUID customerId, String lastName, String firstName, String birthDate, String phoneNumber, String email){
        this.customerId = customerId;
        this.lastName = lastName;
        this.firstName = firstName;
        this.birthDate = birthDate;
        this.phoneNumber = phoneNumber;
        this.email = email;
    }




}
