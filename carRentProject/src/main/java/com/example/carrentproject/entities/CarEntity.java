package com.example.carrentproject.entities;

import com.example.carrentproject.enums.Brand;
import com.example.carrentproject.enums.FuelType;
import com.example.carrentproject.enums.ModelName;
import com.fasterxml.jackson.annotation.JsonFormat;
import jakarta.persistence.*;
import lombok.*;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Entity
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "CarEntity")

public class CarEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    private UUID carId;
    @Enumerated(EnumType.STRING)
    private Brand brand;
    @Enumerated(EnumType.STRING)
    private ModelName modelName;
    private Long mileage;
    @Enumerated(EnumType.STRING)
    private FuelType fuelType;
    private String finNumber;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private String whenBought;
    private String buildDate;
    private int kw;
    private String contractTerm;
    private Double monthlyRate;
    @ElementCollection
    private List<String> carProperties;

    @OneToMany(mappedBy = "car")
    private List<ContractEntity> contractEntities = new ArrayList<>();
    @OneToMany(mappedBy = "car")
    private List<RatingEntity> ratingEntities = new ArrayList<>();




}
