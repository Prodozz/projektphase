package com.example.carrentproject.entities;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.UUID;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class MotorcarAccidentEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    private UUID motorcarAccidentId;

    private String accidentHeader;
    private String accidentDescriptionText;

    @OneToOne
    @JoinColumn(name = "customerId")
    private CustomerEntity customer;

    @OneToOne
    @JoinColumn(name = "employeeId")
    private EmployeeEntity employee;



}
