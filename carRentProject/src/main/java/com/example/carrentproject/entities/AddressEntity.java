package com.example.carrentproject.entities;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.UUID;

@Entity
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class AddressEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    private UUID addressId;
    private String state;
    private String district;
    private String streetName;
    private int streetNumber;
    private int houseNumber;
    private int door;


    @OneToOne(mappedBy = "address")
    CustomerEntity customerEntity;

    @OneToOne(mappedBy = "address")
    EmployeeEntity employeeEntity;


}
