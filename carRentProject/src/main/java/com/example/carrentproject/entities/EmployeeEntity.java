package com.example.carrentproject.entities;

import com.example.carrentproject.enums.Employment_Type;
import com.example.carrentproject.enums.Position;
import com.example.carrentproject.security.entities.SecurityUserEntity;
import jakarta.persistence.*;
import lombok.*;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.Collection;
import java.util.List;
import java.util.UUID;

@Entity
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class EmployeeEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    private UUID employeeId;
    private Double grossSalary;
    @Enumerated(EnumType.STRING)
    private Employment_Type employmentType;
    @Enumerated(EnumType.STRING)
    private Position position;
    private String phoneNumber;
    private String birthDate;
    private String firstName;
    private String lastName;
    private String username;


    @OneToOne
    @JoinColumn(name = "addressId")
    private AddressEntity address;
    @OneToOne(mappedBy = "employee")
    MotorcarAccidentEntity motorcarAccidentEntity;
    @OneToOne(mappedBy = "employee")
    ContractEntity contractEntity;

    public EmployeeEntity(UUID employeeId, String lastName, String firstName, String birthDate, String phoneNumber, Double grossSalary, Employment_Type employmentType, Position position){
        this.employeeId = employeeId;
        this.lastName = lastName;
        this.firstName = firstName;
        this.birthDate = birthDate;
        this.phoneNumber = phoneNumber;
        this.grossSalary = grossSalary;
        this.employmentType = employmentType;
        this.position = position;
    }

}
