package com.example.carrentproject.entities;

import com.example.carrentproject.enums.IntervalOfPayments;
import jakarta.persistence.*;
import lombok.*;

import java.util.UUID;

@Entity
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Data
@Table(name = "Contract_PaymentStatusEntity")
public class Contract_PaymentStatusEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    private UUID contractPaymentStatusId;
    @Enumerated(EnumType.STRING)
    private IntervalOfPayments intervalOfPayments;
    private String intervalRate;

    @OneToOne
    @JoinColumn(name = "contractId")
    private ContractEntity contract;

}
