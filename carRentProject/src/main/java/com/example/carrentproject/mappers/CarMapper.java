package com.example.carrentproject.mappers;

import com.example.carrentproject.DTOs.carDTOs.CarDTO;
import com.example.carrentproject.DTOs.carDTOs.CreateCarDTO;
import com.example.carrentproject.entities.CarEntity;
import com.example.carrentproject.entities.ContractEntity;
import com.example.carrentproject.entities.RatingEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Named;
import org.mapstruct.factory.Mappers;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring", uses = {UUID.class})
public interface CarMapper {

    CarMapper CAR_MAPPER = Mappers.getMapper(CarMapper.class);

    @Mapping(source = "contractEntities", target = "contractEntities", qualifiedByName = "contractEntitiesToStrings")
    @Mapping(source = "ratingEntities", target = "ratingEntities", qualifiedByName = "ratingEntitiesToStrings")
    CarDTO toDto(CarEntity carEntity);

    @Mapping(target = "carId", ignore = true)
    @Mapping(target = "contractEntities", ignore = true)
    @Mapping(target = "ratingEntities", ignore = true)
    CarEntity toEntity(CreateCarDTO createCarDTO);

    @Mapping(target = "carId", ignore = true)
    @Mapping(target = "contractEntities", ignore = true)
    @Mapping(target = "ratingEntities", ignore = true)
    void updateEntity(CarDTO carDTO, @MappingTarget CarEntity carEntity);


    @Named("contractEntitiesToStrings")
    default List<String> contractEntitiesToStrings(List<ContractEntity> contractEntities) {
        return contractEntities == null || contractEntities.isEmpty() ? null :
                contractEntities.stream().map(ContractEntity::getContractId).map(UUID::toString).collect(Collectors.toList());

    }

    @Named("ratingEntitiesToStrings")
    default List<String> ratingEntitiesToStrings(List<RatingEntity> ratingEntities) {
        return ratingEntities == null || ratingEntities.isEmpty() ? null :
                ratingEntities.stream().map(RatingEntity::getRatingId).map(UUID::toString).collect(Collectors.toList());
    }




}
