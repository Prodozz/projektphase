package com.example.carrentproject.mappers;

import com.example.carrentproject.DTOs.adressDTOs.AddressDTO;
import com.example.carrentproject.entities.AddressEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

import java.util.UUID;


@Mapper(componentModel = "spring", uses = {UUID.class})
public interface AddressMapper {

    AddressMapper ADDRESS_MAPPER = Mappers.getMapper(AddressMapper.class);


    @Mapping(source = "addressEntity.customerEntity.customerId", target = "customerId")
    @Mapping(source = "addressEntity.employeeEntity.employeeId", target = "employeeId" )
    AddressDTO toDto(AddressEntity addressEntity);

    @Mapping(target = "addressId", ignore = true)
    @Mapping(target = "customerEntity", ignore = true)
    @Mapping(target = "employeeEntity", ignore = true)
    AddressEntity toEntity(AddressDTO addressDTO);


    @Mapping(target = "addressId", ignore = true)
    @Mapping(target = "customerEntity", ignore = true)
    @Mapping(target = "employeeEntity", ignore = true)
    void updateEntity(AddressDTO addressDTO, @MappingTarget AddressEntity addressEntity);




}
