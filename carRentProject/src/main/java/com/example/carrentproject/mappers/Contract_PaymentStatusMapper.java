package com.example.carrentproject.mappers;

import com.example.carrentproject.DTOs.contractPaymentStatusDTOs.Contract_PaymentStatusDTO;
import com.example.carrentproject.entities.Contract_PaymentStatusEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

import java.util.UUID;

@Mapper(componentModel = "spring", uses = {UUID.class})
public interface Contract_PaymentStatusMapper {

    Contract_PaymentStatusMapper CONTRACT_PAYMENT_STATUS_MAPPER = Mappers.getMapper(Contract_PaymentStatusMapper.class);

    @Mapping(source = "contractPaymentStatusEntity.contract.contractId", target = "contractId")
    Contract_PaymentStatusDTO toDto(Contract_PaymentStatusEntity contractPaymentStatusEntity);

    @Mapping(target = "contractPaymentStatusId", ignore = true)
    @Mapping(target = "contract", ignore = true)
    Contract_PaymentStatusEntity toEntity(Contract_PaymentStatusDTO contractPaymentStatusDTO);

    @Mapping(target = "contractPaymentStatusId", ignore = true)
    @Mapping(target = "contract", ignore = true)
    void updateEntity(Contract_PaymentStatusDTO contractPaymentStatusDTO,
                      @MappingTarget Contract_PaymentStatusEntity contractPaymentStatusEntity);



}
