package com.example.carrentproject.mappers;

import com.example.carrentproject.DTOs.customerDTOs.CustomerUserPropertiesDTO;
import com.example.carrentproject.DTOs.employeeDTOs.EmployeeUserPropertiesDTO;
import com.example.carrentproject.DTOs.employeeDTOs.FullEmployeeDTO;
import com.example.carrentproject.DTOs.employeeDTOs.RegisterEmployeeDTO;
import com.example.carrentproject.entities.EmployeeEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

import java.util.UUID;


@Mapper(componentModel = "spring", uses = {UUID.class, UserMapper.class})
public interface EmployeeMapper {

    EmployeeMapper EMPLOYEE_MAPPER = Mappers.getMapper(EmployeeMapper.class);

    @Mapping(source = "employeeEntity.address.addressId", target = "addressId")
    @Mapping(source = "employeeEntity.motorcarAccidentEntity.motorcarAccidentId", target = "motorcarAccidentId")
    @Mapping(source = "employeeEntity.contractEntity.contractId", target = "contractId")
    FullEmployeeDTO toDto(EmployeeEntity employeeEntity);

    EmployeeEntity registerEmployee(RegisterEmployeeDTO registerEmployeeDTO);

    EmployeeUserPropertiesDTO EntityToCredentialsDTO(EmployeeEntity employeeEntity);


    @Mapping(target = "employeeId", ignore = true)
    @Mapping(target = "address", ignore = true)
    @Mapping(target = "motorcarAccidentEntity", ignore = true)
    @Mapping(target = "contractEntity", ignore = true)
    void updateEntity(EmployeeUserPropertiesDTO employeeUserPropertiesDTO, @MappingTarget EmployeeEntity employeeEntity);



}
