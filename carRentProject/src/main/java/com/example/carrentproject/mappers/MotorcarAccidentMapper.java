package com.example.carrentproject.mappers;

import com.example.carrentproject.DTOs.motorcarAccidentDTOs.MotorcarAccidentDTO;
import com.example.carrentproject.entities.MotorcarAccidentEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

import java.util.UUID;

@Mapper(componentModel = "spring", uses = {UUID.class})
public interface MotorcarAccidentMapper {

    MotorcarAccidentMapper MOTORCAR_ACCIDENT_MAPPER = Mappers.getMapper(MotorcarAccidentMapper.class);


    @Mapping(source = "motorcarAccidentEntity.customer.customerId", target = "customerId")
    @Mapping(source= "motorcarAccidentEntity.employee.employeeId", target = "employeeId")
    MotorcarAccidentDTO toDto(MotorcarAccidentEntity motorcarAccidentEntity);

    @Mapping(target = "motorcarAccidentId", ignore = true)
    @Mapping(target = "customer", ignore = true)
    @Mapping(target = "employee", ignore = true)
    MotorcarAccidentEntity toEntity(MotorcarAccidentDTO motorcarAccidentDTO);

    @Mapping(target = "motorcarAccidentId", ignore = true)
    @Mapping(target = "customer", ignore = true)
    @Mapping(target = "employee", ignore = true)
    void updateEntity(MotorcarAccidentDTO motorcarAccidentDTO, @MappingTarget MotorcarAccidentEntity motorcarAccidentEntity);


}
