package com.example.carrentproject.mappers;

import com.example.carrentproject.DTOs.userDTOs.UserDTO;
import com.example.carrentproject.config.UserAuthority;
import com.example.carrentproject.entities.UserEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.mapstruct.ObjectFactory;
import org.mapstruct.factory.Mappers;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.List;
import java.util.stream.Collectors;

import static com.example.carrentproject.config.UserAuthority.fromStringToAuthority;

@Mapper(componentModel = "spring", uses = {SimpleGrantedAuthority.class})
public interface UserMapper {

    UserMapper USER_MAPPER = Mappers.getMapper(UserMapper.class);

    @Mapping(source = "authority", target = "authority", qualifiedByName = "fromAuthorityToString")
    UserDTO toDto(UserEntity userEntity);

    @ObjectFactory
    @Mapping(source = "authority", target = "authority", qualifiedByName = "fromStringToAuthority")
    UserEntity createUserEntity(UserDTO userDTO);

    @Mapping(source = "authority", target = "authority", qualifiedByName = "fromStringToAuthority")
    UserEntity toEntity(UserDTO userDTO);


    @Named("fromStringToAuthority")
    default List<UserAuthority> fromStringToAuth(List<String> authority){
        return authority.stream().map(UserAuthority::fromStringToAuthority).collect(Collectors.toList());
    }
    @Named("fromAuthorityToString")
    default List<String> fromAuthorityToString(List<UserAuthority> authority){
        return authority.stream().map(UserAuthority::getAuthority).collect(Collectors.toList());
    }

}
