package com.example.carrentproject.mappers;

import com.example.carrentproject.DTOs.RatingDTOs.RatingDTO;
import com.example.carrentproject.entities.RatingEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

import java.util.UUID;

@Mapper(componentModel = "spring", uses = {UUID.class})
public interface RatingMapper {

    RatingMapper RATING_MAPPER = Mappers.getMapper(RatingMapper.class);


    @Mapping(source = "ratingEntity.customer.customerId", target = "customerId" )
    @Mapping(source = "ratingEntity.car.carId", target = "carId")
    RatingDTO toDto(RatingEntity ratingEntity);

    @Mapping(target = "ratingId", ignore = true)
    @Mapping(target = "customer", ignore = true)
    @Mapping(target = "car", ignore = true)
    RatingEntity toEntity(RatingDTO ratingDTO);

    @Mapping(target = "ratingId", ignore = true)
    @Mapping(target = "customer", ignore = true)
    @Mapping(target = "car", ignore = true)
    void updateEntity(RatingDTO ratingDTO, @MappingTarget RatingEntity ratingEntity);

}
