package com.example.carrentproject.mappers;

import com.example.carrentproject.DTOs.contractDTOs.ContractDTO;
import com.example.carrentproject.entities.ContractEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

import java.util.UUID;

@Mapper(componentModel = "spring", uses = {UUID.class, Boolean.class})
public interface ContractMapper {

    ContractMapper CONTRACT_MAPPER = Mappers.getMapper(ContractMapper.class);


    @Mapping(source = "contractEntity.car.carId", target = "carId")
    @Mapping(source = "contractEntity.customer.customerId", target = "customerId")
    @Mapping(source = "contractEntity.employee.employeeId", target = "employeeId")
    @Mapping(source = "contractEntity.contractPaymentStatusEntity.contractPaymentStatusId", target = "contractPaymentStatusId")
    ContractDTO toDto(ContractEntity contractEntity);

    @Mapping(target = "contractId", ignore = true)
    @Mapping(target = "car", ignore = true)
    @Mapping(target = "customer", ignore = true)
    @Mapping(target = "employee", ignore = true)
    @Mapping(target = "contractPaymentStatusEntity", ignore = true)
    ContractEntity toEntity(ContractDTO contractDTO);

    @Mapping(target = "contractId", ignore = true)
    @Mapping(target = "car", ignore = true)
    @Mapping(target = "customer", ignore = true)
    @Mapping(target = "employee", ignore = true)
    @Mapping(target = "contractPaymentStatusEntity", ignore = true)
    void updateEntity(ContractDTO contractDTO, @MappingTarget ContractEntity contractEntity);



}
