package com.example.carrentproject.mappers;

import com.example.carrentproject.DTOs.customerDTOs.CustomerUserPropertiesDTO;
import com.example.carrentproject.DTOs.customerDTOs.RegisterCustomerDTO;
import com.example.carrentproject.DTOs.customerDTOs.FullCustomerDTO;
import com.example.carrentproject.entities.CustomerEntity;
import com.example.carrentproject.entities.RatingEntity;
import org.mapstruct.*;
import org.mapstruct.factory.Mappers;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring", uses = {UUID.class, UserMapper.class})
public interface CustomerMapper {

    CustomerMapper CUSTOMER_MAPPER = Mappers.getMapper(CustomerMapper.class);

    @Mapping(source = "customerEntity.address.addressId", target = "addressId")
    @Mapping(source = "customerEntity.motorcarAccidentEntity.motorcarAccidentId", target = "motorcarAccidentId")
    @Mapping(source = "customerEntity.contractEntity.contractId", target = "contractId")
    @Mapping(source = "customerEntity.ratingEntities", target = "ratingEntityIds", qualifiedByName = "ratingEntitiesToStrings")
    FullCustomerDTO toDto(CustomerEntity customerEntity);

    @Mapping(target = "customerId", ignore = true)
    CustomerEntity createCustomerEntity(RegisterCustomerDTO registerCustomerDTO);

    @Mapping(target = "customerId", ignore = true)
//    @Mapping(target = "user", ignore = true)
//    @Mapping(target = "address", ignore = true)
//    @Mapping(target = "motorcarAccidentEntity", ignore = true)
//    @Mapping(target = "contractEntity", ignore = true)
//    @Mapping(target = "ratingEntities", ignore = true)
    void updateEntity(CustomerUserPropertiesDTO customerUserPropertiesDTO, @MappingTarget CustomerEntity customerEntity);

    @Mapping(source = "customerId", target = "customerId")
    CustomerUserPropertiesDTO EntityToCredentialsDTO(CustomerEntity customerEntity);



    @Named("ratingEntitiesToStrings")
    default List<String> ratingEntitiesToStrings(List<RatingEntity> ratingEntities) {
        return ratingEntities == null || ratingEntities.isEmpty() ? null :
                ratingEntities.stream().map(RatingEntity::getRatingId).map(UUID::toString).collect(Collectors.toList());
    }

}

