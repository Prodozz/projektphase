package com.example.carrentproject.config;

import org.springframework.security.core.GrantedAuthority;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum UserAuthority implements GrantedAuthority {

    CUSTOMER("CUSTOMER"),
    EMPLOYEE("EMPLOYEE"),
    ADMIN("ADMIN");

    private final String authority;

    @Override
    public String getAuthority() {
        return authority;
    }

    public static UserAuthority fromStringToAuthority(String authorityString) {
        for (UserAuthority authority : UserAuthority.values()) {
            if (authority.getAuthority().equals(authorityString)) {
                return authority;
            }
        }
        throw new IllegalArgumentException("Error!");
    }
}
