package com.example.carrentproject.config;

import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.userdetails.jdbc.JdbcDaoImpl;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.JdbcUserDetailsManager;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;

import static org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType.H2;
import static org.springframework.security.config.Customizer.withDefaults;


@Configuration
@EnableWebSecurity
@AllArgsConstructor
@EnableMethodSecurity
public class SecurityConfiguration {

    @Bean
    UserDetailsManager userDetailsManager(DataSource dataSource) {

        return new JdbcUserDetailsManager(dataSource);
    }
    @Bean
    DataSource dataSource() {
        return new EmbeddedDatabaseBuilder()
                .setType(H2)
                .addScript(JdbcDaoImpl.DEFAULT_USER_SCHEMA_DDL_LOCATION)
                .build();
    }

    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {

        return http
                .formLogin().and()
                .httpBasic(withDefaults())
                .authorizeHttpRequests(authz ->
                        authz
//                                .requestMatchers("/h2-console", "/h2-console/**").permitAll()
//                                .requestMatchers(POST, "/api/customer/register").permitAll()
                                .anyRequest().permitAll()
//                                .requestMatchers("/h2-console", "/h2-console/**" ).permitAll()
//                                .requestMatchers(GET,"/api/employee").hasAnyRole(EMPLOYEE.getRole())
//                                .requestMatchers(POST, "/api/employee").hasAnyRole(ADMIN.getRole())
//                                .anyRequest().authenticated()
                )
                .csrf().disable()
                .headers().disable()
                .build();
//                .anyRequest().access(new WebExpressionAuthorizationManager("@webSecurity.check(authentication, request)")) // SpEL --> authorization rules


    }

//    @Bean
//    UserDetailsService userDetailsService() {
//
//        var db = new JdbcUserDetailsManager();
//
//        var user = User
//                .builder()
//                .username("Mansur")
//                .password(passwordEncoder().encode("12345"))
//                .roles(ADMIN.getAuthority(), EMPLOYEE.getAuthority())
//                .build();
//
//        var user2 = User
//                .builder()
//                .username("daud")
//                .password(passwordEncoder().encode("12345"))
//                .roles(CUSTOMER.getAuthority())
//                .build();
//
//
//        db.createUser(user);
//        db.createUser(user2);
//        return db;
//    }


//    @Bean
//    public UserDetailsService userDetailsService() {
//        return new CustomUserDetailsManager();
//    }

    @Bean
    PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

//    @Bean
//    public RegisteredClientRepository registeredClientRepository(){
//
//        RegisteredClient r1 = RegisteredClient.withId(UUID.randomUUID().toString())
//                .clientId("client")
//                .clientSecret("secret")
//                .scope(OidcScopes.OPENID)
//                .scope(OidcScopes.PROFILE)
//                .redirectUri("https://springone.com/authorized")
//                .clientAuthenticationMethod(ClientAuthenticationMethod.CLIENT_SECRET_BASIC)
//                .authorizationGrantType(AuthorizationGrantType.AUTHORIZATION_CODE)
//                .authorizationGrantType(AuthorizationGrantType.REFRESH_TOKEN)
//                .build();
//
//        return new InMemoryRegisteredClientRepository(r1);
//    }
//
//    @Bean
//    public AuthorizationServerSettings authorizationServerSettings(){
//        return AuthorizationServerSettings.builder().build();
//    }

}
