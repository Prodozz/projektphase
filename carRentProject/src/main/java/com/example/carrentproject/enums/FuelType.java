package com.example.carrentproject.enums;

public enum FuelType {

    DIESEL("Diesel"),
    PETROL("Benzin"),
    ELECTRICITY("Elektro"),
    NIX("Nix");

    private final String name;

    FuelType(String name) {
        this.name = name;
    }
    public String getName(){
        return name;
    }

    public static FuelType stringToEnum(String fuel_type) {
        for (FuelType fuelTypes : FuelType.values()) {
            if (fuelTypes.getName().equals(fuel_type)) {
                return fuelTypes;
            }
        }

        return NIX;
    }

}
