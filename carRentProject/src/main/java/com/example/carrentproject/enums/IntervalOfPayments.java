package com.example.carrentproject.enums;

public enum IntervalOfPayments {

    BI_WEEKLY,
    MONTHLY,
    QUARTERLY,
    SEMIANUALLY


}
