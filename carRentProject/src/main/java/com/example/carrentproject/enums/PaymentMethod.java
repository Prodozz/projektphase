package com.example.carrentproject.enums;

public enum PaymentMethod {

    MASTERCARD,
    VISA,
    BILL;

    public static PaymentMethod getPaymentMethodFromString(String paymentMethod) {
        for (PaymentMethod method : PaymentMethod.values()) {
            if (method.name().equalsIgnoreCase(paymentMethod)) {
                return method;
            }
        }
        return null;
    }


    }
