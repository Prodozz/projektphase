package com.example.carrentproject.enums;

public enum Status {

    PENDING("Ausstehend"),
    VERIFIED("Verifiziert"),
    NOT_VERIFIED("Nicht verifiziert");

    private final String name;

    Status(String name){
        this.name = name;
    }

    public String getName(){
        return name;
    }

    public static Status stringToEnum(String status) {
        for (Status status1 : Status.values()) {
            if (status1.getName().equals(status)) {
                return status1;
            }
        }
        return Status.NOT_VERIFIED;
    }

}
