package com.example.carrentproject.enums;

public enum Brand {

    AUDI("Audi"),
    MERCEDES("Mercedes"),
    BMW("BMW"),
    VW("VW"),
    TESLA("Tesla"),
    NIX("Nix");

    private final String name;

    Brand(String name) {
        this.name = name;
    }
    public String getName(){
        return name;
    }

    public static Brand stringToEnum(String brand){
        for (Brand brand1 : Brand.values()) {
            if (brand1.getName().equals(brand)){
                return brand1;
            }
        }
        return NIX;
    }

}
