package com.example.carrentproject.enums;

public enum Employment_Type {

    FULLTIME("Vollzeit"),
    PARTTIME("Teilzeit"),
    NIX("Nix");

    private final String name;

    Employment_Type(String name) {
        this.name = name;
    }
    public String getName(){
        return name;
    }

   public static Employment_Type stringToEmploymentTypeEnum(String employment_type){
        for (Employment_Type employment_Types : Employment_Type.values()) {
            if (employment_Types.getName().equals(employment_type)) {
                return employment_Types;
            }
        }

        return NIX;
   }
}
