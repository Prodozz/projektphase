package com.example.carrentproject.DTOs.userDTOs;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.repository.cdi.Eager;

@Getter
@Setter
public class ChangePasswordDTO {

    private String oldPassword;
    private String newPassword;
}
