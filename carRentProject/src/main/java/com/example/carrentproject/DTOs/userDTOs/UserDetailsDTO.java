package com.example.carrentproject.DTOs.userDTOs;

import lombok.Data;

import java.util.List;

@Data
public class UserDetailsDTO {

    private String username;
    private String password;
    private boolean enabled;
    private List<String> authorities;


}
