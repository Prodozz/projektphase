package com.example.carrentproject.DTOs.RatingDTOs;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@Data
public class RatingDTO {

    private String ratingId;
    private Integer rating;
    private String feedbackText;
    private String customerId;
    private String carId;
}
