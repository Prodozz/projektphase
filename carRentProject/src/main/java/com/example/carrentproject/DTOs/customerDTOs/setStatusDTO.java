package com.example.carrentproject.DTOs.customerDTOs;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class setStatusDTO {

    private String customerId;
    private String status;
    private Boolean hasCreditWorthiness;

}
