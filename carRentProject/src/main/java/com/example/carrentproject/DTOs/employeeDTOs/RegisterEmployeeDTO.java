package com.example.carrentproject.DTOs.employeeDTOs;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class RegisterEmployeeDTO {

    private String username;
    private String password;
    private String firstName;
    private String lastName;
    private String phoneNumber;
    private String birthDate;
    private Double grossSalary;
    private String employmentType;
    private String position;

}
