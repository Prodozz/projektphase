package com.example.carrentproject.DTOs.carDTOs;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@Data
public class CarDTO {

    private String carId;
    private String brand;
    private String modelName;
    private Long mileage;
    private String fuelType;
    private String finNumber;
    private String whenBought;
    private String buildDate;
    private Integer kw;
    private String contractTerm;
    private Double monthlyRate;
    private List<String> carProperties;
    private List<String> contractEntities;
    private List<String> ratingEntities;
}
