package com.example.carrentproject.DTOs.carDTOs;

import lombok.Data;

import java.util.List;

@Data
public class CreateCarDTO {
    private String brand;
    private String modelName;
    private Long mileage;
    private String fuelType;
    private String finNumber;
    private String whenBought;
    private String buildDate;
    private Integer kw;
    private String contractTerm;
    private Double monthlyRate;
    private List<String> carProperties;
    private String contractEntity;
}
