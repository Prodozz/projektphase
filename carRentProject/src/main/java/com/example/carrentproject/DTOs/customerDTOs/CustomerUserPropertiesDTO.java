package com.example.carrentproject.DTOs.customerDTOs;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CustomerUserPropertiesDTO {

    private String customerId;
    private String phoneNumber;
    private String email;
    private String birthDate;
    private String firstName;
    private String lastName;
}
