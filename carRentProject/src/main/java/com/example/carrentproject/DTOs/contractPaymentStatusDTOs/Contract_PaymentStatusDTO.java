package com.example.carrentproject.DTOs.contractPaymentStatusDTOs;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@Data
public class Contract_PaymentStatusDTO {
    private String contractPaymentStatusId;
    private String intervalOfPayments;
    private String intervalRate;
    private String contractId;
}
