package com.example.carrentproject.DTOs.userDTOs;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;


@AllArgsConstructor
@Data
public class UserDTO {

    private String phoneNumber;
    private String birthDate;
    private String firstName;
    private String lastName;
    private String username;
    private String password;
    private List<String> authority;


}
