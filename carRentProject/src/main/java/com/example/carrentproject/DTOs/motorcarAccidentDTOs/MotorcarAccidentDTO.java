package com.example.carrentproject.DTOs.motorcarAccidentDTOs;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MotorcarAccidentDTO {

    private String motorcarAccidentId;
    private String accidentHeader;
    private String accidentDescriptionText;
    private String customerId;
    private String employeeId;

}
