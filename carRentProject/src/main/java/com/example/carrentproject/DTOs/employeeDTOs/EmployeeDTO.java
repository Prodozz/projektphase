package com.example.carrentproject.DTOs.employeeDTOs;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@Data
public class EmployeeDTO {

    private String employeeId;
    private String email;
    private String phoneNumber;
    private Double grossSalary;
    private String password;
    private String birthDate;
    private String firstName;
    private String lastName;
    private String userAuthority;
    private String employmentType;
    private String position;
    private String addressId;
    private String motorcarAccidentId;
    private String contractId;
}
