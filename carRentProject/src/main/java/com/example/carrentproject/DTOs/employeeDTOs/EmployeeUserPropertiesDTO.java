package com.example.carrentproject.DTOs.employeeDTOs;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class EmployeeUserPropertiesDTO {

    private String employeeId;
    private String phoneNumber;
    private String birthDate;
    private String firstName;
    private String lastName;
    private String grossSalary;
    private String employmentType;
    private String position;

}
