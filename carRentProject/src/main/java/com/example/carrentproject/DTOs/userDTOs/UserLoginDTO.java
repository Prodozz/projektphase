package com.example.carrentproject.DTOs.userDTOs;

import lombok.Data;

@Data
public class UserLoginDTO {
    private String username, password;
}
