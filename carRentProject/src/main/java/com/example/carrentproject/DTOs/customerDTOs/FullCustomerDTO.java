package com.example.carrentproject.DTOs.customerDTOs;


import lombok.*;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class FullCustomerDTO {

    private String customerId;
    private String username;
    private String phoneNumber;
    private String birthDate;
    private String firstName;
    private String lastName;
    private String status;
    private String paymentMethod;
    private Boolean hasCreditWorthiness;
    private String addressId;
    private String motorcarAccidentId;
    private String contractId;
    private List<String> ratingEntityIds;


}
