package com.example.carrentproject.DTOs.customerDTOs;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class RegisterCustomerDTO {

    private String username;
    private String email;
    private String password;
    private String phoneNumber;
    private String birthDate;
    private String firstName;
    private String lastName;

}
