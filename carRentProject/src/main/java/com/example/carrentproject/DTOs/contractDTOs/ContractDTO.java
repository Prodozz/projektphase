package com.example.carrentproject.DTOs.contractDTOs;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@Data

public class ContractDTO {

    private String contractId;
    private String rentStartDate;
    private String rentEndDate;
    private String contractDate;
    private Double monthlyRate;
    private String contractTerm;
    private Boolean isActive;
    private int rating;
    private String feedbackText;
    private String carId;
    private String contractPaymentStatusId;
    private String customerId;
    private String employeeId;
}
