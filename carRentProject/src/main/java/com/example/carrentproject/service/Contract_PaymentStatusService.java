package com.example.carrentproject.service;

import com.example.carrentproject.DTOs.contractPaymentStatusDTOs.Contract_PaymentStatusDTO;
import com.example.carrentproject.entities.ContractEntity;
import com.example.carrentproject.entities.Contract_PaymentStatusEntity;
import com.example.carrentproject.mappers.Contract_PaymentStatusMapper;
import com.example.carrentproject.repository.ContractCRUDRepository;
import com.example.carrentproject.repository.Contract_PaymentStatusCRUDRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class Contract_PaymentStatusService {

    private Contract_PaymentStatusMapper contractPaymentStatusMapper;
    private Contract_PaymentStatusCRUDRepository contractPaymentStatusCRUDRepository;
    private ContractCRUDRepository contractCRUDRepository;

    public List<Contract_PaymentStatusDTO> getAllContract_PaymentStatus() {

        List<Contract_PaymentStatusEntity> contractPaymentStatusEntities =
                (List<Contract_PaymentStatusEntity>) contractPaymentStatusCRUDRepository.findAll();

        return contractPaymentStatusEntities.stream().map(contractPaymentStatusEntity ->
                contractPaymentStatusMapper.toDto(contractPaymentStatusEntity)).collect(Collectors.toList());
    }


    public Contract_PaymentStatusDTO getContract_PaymentStatus(String contract_paymentstatusId) {

        Contract_PaymentStatusEntity contractPaymentStatusEntity = contractPaymentStatusCRUDRepository
                .findByContractPaymentStatusId(UUID.fromString(contract_paymentstatusId))
                .orElseThrow(() -> new NoSuchElementException("Error!"));

        return contractPaymentStatusMapper.toDto(contractPaymentStatusEntity);

    }

    public void createContract_PaymentStatus(Contract_PaymentStatusDTO contract_paymentstatusDTO) {

        Contract_PaymentStatusEntity contractPaymentStatusEntity = contractPaymentStatusMapper.toEntity(contract_paymentstatusDTO);
        contractPaymentStatusMapper.updateEntity(contract_paymentstatusDTO, contractPaymentStatusEntity);

        ContractEntity contractEntity = null;

        if (contract_paymentstatusDTO.getContractId() != null) {
            contractEntity = contractCRUDRepository
                    .findByContractId(UUID.fromString(contract_paymentstatusDTO.getContractId()))
                    .orElseThrow(() -> new NoSuchElementException("Error"));
        }

        contractPaymentStatusEntity.setContract(contractEntity);

        contractPaymentStatusCRUDRepository.save(contractPaymentStatusEntity);

    }

    public void deleteContract_PaymentStatus(String contract_paymentstatusId) throws NoSuchElementException {

        Contract_PaymentStatusEntity contractPaymentStatusEntity = contractPaymentStatusCRUDRepository
                .findByContractPaymentStatusId(UUID.fromString(contract_paymentstatusId))
                .orElseThrow(() -> new NoSuchElementException("Error"));


        if (contractPaymentStatusEntity.getContract() != null) {
            ContractEntity contract = contractCRUDRepository
                    .findByContractId(contractPaymentStatusEntity.getContract().getContractId())
                    .orElseThrow(() -> new NoSuchElementException("Error!"));
            contract.setContractPaymentStatusEntity(null);
            contractCRUDRepository.save(contract);
        }

        contractPaymentStatusCRUDRepository.deleteByContractPaymentStatusId(UUID.fromString(contract_paymentstatusId));

    }

    public void updateContract_PaymentStatus(Contract_PaymentStatusDTO contract_paymentstatusDTO) {

        Contract_PaymentStatusEntity contractPaymentStatusEntity = contractPaymentStatusCRUDRepository
                .findByContractPaymentStatusId(UUID.fromString(contract_paymentstatusDTO.getContractPaymentStatusId()))
                .orElseThrow(() -> new NoSuchElementException("Error!"));

        contractPaymentStatusMapper.updateEntity(contract_paymentstatusDTO, contractPaymentStatusEntity);

        ContractEntity contractEntity = contractCRUDRepository
                .findByContractId(UUID.fromString(contract_paymentstatusDTO.getContractId()))
                .orElse(null);

        contractPaymentStatusEntity.setContract(contractEntity);

        contractPaymentStatusCRUDRepository.save(contractPaymentStatusEntity);

    }
}
