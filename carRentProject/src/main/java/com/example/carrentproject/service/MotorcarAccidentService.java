package com.example.carrentproject.service;

import com.example.carrentproject.DTOs.motorcarAccidentDTOs.MotorcarAccidentDTO;
import com.example.carrentproject.entities.CustomerEntity;
import com.example.carrentproject.entities.EmployeeEntity;
import com.example.carrentproject.entities.MotorcarAccidentEntity;
import com.example.carrentproject.mappers.MotorcarAccidentMapper;
import com.example.carrentproject.repository.CustomerCRUDRepository;
import com.example.carrentproject.repository.EmployeeCRUDRepository;
import com.example.carrentproject.repository.MotorcarAccidentCRUDRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class MotorcarAccidentService {

    MotorcarAccidentCRUDRepository motorcarAccidentCRUDRepository;
    CustomerCRUDRepository customerCRUDRepository;
    EmployeeCRUDRepository employeeCRUDRepository;
    MotorcarAccidentMapper motorcarAccidentMapper;

    public List<MotorcarAccidentDTO> getAllMotorcarAccidents() {

        List<MotorcarAccidentEntity> motorcarAccidentEntities =
                (List<MotorcarAccidentEntity>) motorcarAccidentCRUDRepository.findAll();

        return motorcarAccidentEntities.stream()
                .map(motorcarAccidentMapper::toDto).collect(Collectors.toList());

    }


    public MotorcarAccidentDTO getMotorcarAccident(String motorcaraccidentId) {

        MotorcarAccidentEntity motorcarAccidentEntity = motorcarAccidentCRUDRepository
                .findByMotorcarAccidentId(UUID.fromString(motorcaraccidentId))
                .orElseThrow(() -> new NoSuchElementException("Error!"));

        return motorcarAccidentMapper.toDto(motorcarAccidentEntity);

    }

    public void createMotorcarAccident(MotorcarAccidentDTO motorcaraccidentDTO) {

        MotorcarAccidentEntity motorcarAccidentEntity = motorcarAccidentMapper.toEntity(motorcaraccidentDTO);
        motorcarAccidentMapper.updateEntity(motorcaraccidentDTO, motorcarAccidentEntity);

        CustomerEntity customerEntity = customerCRUDRepository
                .findByCustomerId(UUID.fromString(motorcaraccidentDTO.getCustomerId()))
                .orElseThrow(() -> new NoSuchElementException("Error!"));

        EmployeeEntity employeeEntity = employeeCRUDRepository
                .findByEmployeeId(UUID.fromString(motorcaraccidentDTO.getEmployeeId()))
                .orElseThrow(() -> new NoSuchElementException("Error!"));


        motorcarAccidentEntity.setCustomer(customerEntity);
        motorcarAccidentEntity.setEmployee(employeeEntity);

        motorcarAccidentCRUDRepository.save(motorcarAccidentEntity);

    }

    public void deleteMotorcarAccident(String motorcaraccidentId) {

        MotorcarAccidentEntity motorcarAccidentEntity = motorcarAccidentCRUDRepository
                .findByMotorcarAccidentId(UUID.fromString(motorcaraccidentId))
                .orElseThrow(() -> new NoSuchElementException("Error!"));

        CustomerEntity customer = motorcarAccidentEntity.getCustomer();
        customer.setMotorcarAccidentEntity(null);
        customerCRUDRepository.save(customer);

        EmployeeEntity employee = motorcarAccidentEntity.getEmployee();
        employee.setMotorcarAccidentEntity(null);
        employeeCRUDRepository.save(employee);

        motorcarAccidentCRUDRepository.deleteByMotorcarAccidentId(UUID.fromString(motorcaraccidentId));

    }

    public void updateMotorcarAccident(MotorcarAccidentDTO motorcaraccidentDTO) {

        MotorcarAccidentEntity motorcarAccidentEntity = motorcarAccidentCRUDRepository
                .findByMotorcarAccidentId(UUID.fromString(motorcaraccidentDTO.getMotorcarAccidentId()))
                .orElseThrow(() -> new NoSuchElementException("Error!"));

        motorcarAccidentMapper.updateEntity(motorcaraccidentDTO, motorcarAccidentEntity);

        CustomerEntity customerEntity = customerCRUDRepository
                .findByCustomerId(UUID.fromString(motorcaraccidentDTO.getCustomerId()))
                .orElse(null);

        EmployeeEntity employeeEntity = employeeCRUDRepository
                .findByEmployeeId(UUID.fromString(motorcaraccidentDTO.getEmployeeId()))
                .orElse(null);


        motorcarAccidentEntity.setCustomer(customerEntity);
        motorcarAccidentEntity.setEmployee(employeeEntity);

        motorcarAccidentCRUDRepository.save(motorcarAccidentEntity);

    }
}
