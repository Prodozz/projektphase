package com.example.carrentproject.service;

import com.example.carrentproject.DTOs.employeeDTOs.EmployeeUserPropertiesDTO;
import com.example.carrentproject.DTOs.employeeDTOs.FullEmployeeDTO;
import com.example.carrentproject.DTOs.employeeDTOs.RegisterEmployeeDTO;
import com.example.carrentproject.entities.*;
import com.example.carrentproject.mappers.EmployeeMapper;
import com.example.carrentproject.repository.*;
import com.example.carrentproject.security.entities.SecurityUserEntity;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class EmployeeService {

    EmployeeCRUDRepository employeeCRUDRepository;
    AddressCRUDRepository addressCRUDRepository;
    MotorcarAccidentCRUDRepository motorcarAccidentCRUDRepository;
    ContractCRUDRepository contractCRUDRepository;
    EmployeeMapper employeeMapper;
    private final UserCRUDRepository userCRUDRepository;

    public List<FullEmployeeDTO> getAllEmployees() {

        List<EmployeeEntity> employeeEntities = (List<EmployeeEntity>) employeeCRUDRepository.findAll();
        return employeeEntities.stream().map(employeeMapper::toDto).collect(Collectors.toList());

    }

    public EmployeeUserPropertiesDTO getEmployeePropertiesByDTO(RegisterEmployeeDTO registerEmployeeDTO) throws NoSuchElementException {

        EmployeeEntity employeeEntity = employeeCRUDRepository.getEmployeePropertiesByDTO(registerEmployeeDTO)
                .orElseThrow(() -> new NoSuchElementException("Error!"));

        return employeeMapper.EntityToCredentialsDTO(employeeEntity);
    }

    public EmployeeUserPropertiesDTO getEmployeePropertiesById(String employeeId) throws NoSuchElementException {

        EmployeeEntity employeeEntity = employeeCRUDRepository.getEmployeePropertiesByUserId(UUID.fromString(employeeId))
                .orElseThrow(() -> new NoSuchElementException("Error!"));

        return employeeMapper.EntityToCredentialsDTO(employeeEntity);
    }


    public FullEmployeeDTO getEmployee(String employeeId) {

        EmployeeEntity employeeEntity = employeeCRUDRepository.findByEmployeeId(UUID.fromString(employeeId))
                .orElseThrow(() -> new NoSuchElementException("Error!"));

        return employeeMapper.toDto(employeeEntity);

    }


    public void createEmployee(RegisterEmployeeDTO registerEmployeeDTO, String username) {

        EmployeeEntity employeeEntity = employeeMapper.registerEmployee(registerEmployeeDTO);


        employeeEntity.setUsername(username);
        employeeCRUDRepository.save(employeeEntity);

    }

    public String deleteEmployee(String employeeId) {

        EmployeeEntity employee = employeeCRUDRepository.findById(UUID.fromString(employeeId))
                .orElseThrow(() -> new NoSuchElementException("Error!"));


        AddressEntity address = employee.getAddress();
        if (address != null) {
            address.setEmployeeEntity(null);
            addressCRUDRepository.save(address);
        }

//        Accident behält die Customerdaten für Dokumentation

        ContractEntity contract = employee.getContractEntity();
        if (contract != null) {
            contract.setEmployee(null);
            contractCRUDRepository.save(contract);
        }

        employeeCRUDRepository.deleteById(UUID.fromString(employeeId));

        return employee.getUsername();

    }

    public void updateEmployeeProperties(EmployeeUserPropertiesDTO employeeUserDPropertiesDTO) {

        EmployeeEntity employeeEntity = employeeCRUDRepository
                .findByEmployeeId(UUID.fromString(employeeUserDPropertiesDTO.getEmployeeId()))
                .orElseThrow(() -> new NoSuchElementException("Error!"));

        employeeMapper.updateEntity(employeeUserDPropertiesDTO, employeeEntity);

        employeeCRUDRepository.save(employeeEntity);

    }


}
