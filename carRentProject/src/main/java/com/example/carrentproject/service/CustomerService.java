package com.example.carrentproject.service;

import com.example.carrentproject.DTOs.customerDTOs.CustomerUserPropertiesDTO;
import com.example.carrentproject.DTOs.customerDTOs.RegisterCustomerDTO;
import com.example.carrentproject.DTOs.customerDTOs.FullCustomerDTO;
import com.example.carrentproject.DTOs.customerDTOs.SetPaymentDTO;
import com.example.carrentproject.DTOs.userDTOs.UserLoginDTO;
import com.example.carrentproject.entities.*;
import com.example.carrentproject.enums.PaymentMethod;
import com.example.carrentproject.enums.Status;
import com.example.carrentproject.mappers.CustomerMapper;
import com.example.carrentproject.repository.*;
import lombok.AllArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class CustomerService {

    private final CustomerCRUDRepository customerCRUDRepository;
    private final AddressCRUDRepository addressCRUDRepository;
    private final RatingCRUDRepository ratingCRUDRepository;
    private final CustomerMapper customerMapper;
    private final UserCRUDRepository userCRUDRepository;
    private final UserDetailsManager userDetailsManager;
    private final ContractCRUDRepository contractCRUDRepository;
    private final MotorcarAccidentCRUDRepository motorcarAccidentCRUDRepository;
    private final PasswordEncoder passwordEncoder;


    public List<FullCustomerDTO> getAllCustomers() {

        List<CustomerEntity> customerEntities = (List<CustomerEntity>) customerCRUDRepository.findAll();
        return customerEntities.stream().map(customerMapper::toDto).collect(Collectors.toList());
    }


    public CustomerUserPropertiesDTO getCustomerPropertiesByDTO(RegisterCustomerDTO registerCustomerDTO) throws NoSuchElementException {

        CustomerEntity customerEntity = customerCRUDRepository.getCustomerPropertiesByDTO(registerCustomerDTO)
                .orElseThrow(() -> new NoSuchElementException("Error!"));

        return customerMapper.EntityToCredentialsDTO(customerEntity);
    }

    public CustomerUserPropertiesDTO getCustomerPropertiesById(String customerId) throws NoSuchElementException {

        CustomerEntity customerEntity = customerCRUDRepository.getCustomerPropertiesByCustomerId(UUID.fromString(customerId))
                .orElseThrow(() -> new NoSuchElementException("Error!"));

        return customerMapper.EntityToCredentialsDTO(customerEntity);
    }


    public FullCustomerDTO getCustomer(String customerId) throws NoSuchElementException {
        CustomerEntity customerEntity = customerCRUDRepository.findByCustomerId(UUID.fromString(customerId))
                .orElseThrow(() -> new NoSuchElementException("Error!"));

        return customerMapper.toDto(customerEntity);
    }


    public void createCustomer(RegisterCustomerDTO registerCustomerDTO, String username, String email) {

        CustomerEntity customerEntity = customerMapper.createCustomerEntity(registerCustomerDTO);

        customerEntity.setUsername(username);
        customerEntity.setEmail(email);
        customerEntity.setStatus(Status.NOT_VERIFIED);
        customerCRUDRepository.save(customerEntity);

    }

    public String deleteCustomer(String customerId) {
        CustomerEntity customerEntity = customerCRUDRepository.findByCustomerId(UUID.fromString(customerId))
                .orElseThrow(() -> new NoSuchElementException("Error!"));

        AddressEntity address = customerEntity.getAddress();
        if (address != null) {
            address.setCustomerEntity(null);
            addressCRUDRepository.save(address);
        }

        ContractEntity contract = customerEntity.getContractEntity();
        if (contract != null) {
            contract.setCustomer(null);
            contractCRUDRepository.save(contract);
        }

        MotorcarAccidentEntity motorcarAccident = customerEntity.getMotorcarAccidentEntity();
        if (motorcarAccident != null) {
            motorcarAccident.setCustomer(null);
            motorcarAccidentCRUDRepository.save(motorcarAccident);
        }

        if (customerEntity.getRatingEntities() != null && !customerEntity.getRatingEntities().isEmpty()) {
            for (RatingEntity ratingEntity : customerEntity.getRatingEntities()) {
                ratingEntity.setCustomer(null);
                ratingCRUDRepository.save(ratingEntity);
            }
        }


        customerCRUDRepository.deleteById(UUID.fromString(customerId));

        return customerEntity.getUsername();
    }

    public void updateCustomerProperties(CustomerUserPropertiesDTO customerUserPropertiesDTO) {

        CustomerEntity customer = customerCRUDRepository
                .findByCustomerId(UUID.fromString(customerUserPropertiesDTO.getCustomerId()))
                .orElseThrow(() -> new NoSuchElementException("Error!"));

        customerMapper.updateEntity(customerUserPropertiesDTO, customer);
        customerCRUDRepository.save(customer);
    }

    public void setPayment(SetPaymentDTO setPaymentDTO) {

        CustomerEntity customer = customerCRUDRepository.findByCustomerId(UUID.fromString(setPaymentDTO.getCustomerId()))
                .orElseThrow(() -> new NoSuchElementException("Error!"));

        customer.setPaymentMethod(PaymentMethod.getPaymentMethodFromString(setPaymentDTO.getPaymentMethod()));
    }
    public FullCustomerDTO loginCustomer (UserLoginDTO customerDTO) throws Exception {
        UserEntity c = userCRUDRepository.findByUsername(customerDTO.getUsername()).orElseThrow(()-> new NoSuchElementException("User nicht gefunden!")) ;

        if (!c.getPassword().equals(passwordEncoder.encode(customerDTO.getPassword()))){
            throw new Exception("Benutzer oder Passwort ist falsch!");
        }

        CustomerEntity customerEntity = customerCRUDRepository.findByUsername(c.getUsername()).orElseThrow((()-> new NoSuchElementException("Fehler")));

        FullCustomerDTO fullCustomerDTO = customerMapper.toDto(customerEntity);

        return fullCustomerDTO;


    }

}
