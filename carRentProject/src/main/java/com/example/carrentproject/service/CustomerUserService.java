package com.example.carrentproject.service;

import com.example.carrentproject.DTOs.customerDTOs.RegisterCustomerDTO;
import com.example.carrentproject.entities.UserEntity;
import com.example.carrentproject.security.entities.SecurityUserEntity;
import lombok.AllArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.example.carrentproject.config.UserAuthority.CUSTOMER;

@Service
@AllArgsConstructor
public class CustomerUserService {

    private CustomerService customerService;
    private UserDetailsManager userDetailsManager;
    private PasswordEncoder passwordEncoder;

    public void createCustomerUser(RegisterCustomerDTO registerCustomerDTO) {

        SecurityUserEntity securityUserEntity = SecurityUserEntity.builder()
                .user(
                        UserEntity.builder()
                                .username(registerCustomerDTO.getUsername())
                                .email(registerCustomerDTO.getEmail())
                                .password(passwordEncoder.encode(registerCustomerDTO.getPassword()))
                                .enabled(true)
                                .authority(List.of(CUSTOMER))
                                .build()
                ).build();


        userDetailsManager.createUser(securityUserEntity);
        customerService.createCustomer(registerCustomerDTO, registerCustomerDTO.getUsername(), registerCustomerDTO.getEmail());

    }


    public void deleteCustomerUser(String customerId) {
        String customerUsername = customerService.deleteCustomer(customerId);
        userDetailsManager.deleteUser(customerUsername);

    }


}
