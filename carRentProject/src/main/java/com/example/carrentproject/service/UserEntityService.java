package com.example.carrentproject.service;

import com.example.carrentproject.DTOs.userDTOs.UserDetailsDTO;
import com.example.carrentproject.config.UserAuthority;
import com.example.carrentproject.entities.UserEntity;
import com.example.carrentproject.security.entities.SecurityUserEntity;
import lombok.AllArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

import static com.example.carrentproject.config.UserAuthority.CUSTOMER;
import static com.example.carrentproject.config.UserAuthority.fromStringToAuthority;

@Service
@AllArgsConstructor
public class UserEntityService {

    private final PasswordEncoder passwordEncoder;

    public SecurityUserEntity createSecurityUser(UserDetailsDTO userDetailsDTO){
        return SecurityUserEntity.builder()
                .user(
                        UserEntity.builder()
                                .username(userDetailsDTO.getUsername())
                                .password(passwordEncoder.encode(userDetailsDTO.getPassword()))
                                .enabled(true)
                                .authority(userDetailsDTO.getAuthorities().stream().map(UserAuthority::fromStringToAuthority)
                                        .collect(Collectors.toList()))
                                .build()
                ).build();
    }


}
