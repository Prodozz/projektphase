package com.example.carrentproject.service;

import com.example.carrentproject.DTOs.contractDTOs.ContractDTO;
import com.example.carrentproject.entities.*;
import com.example.carrentproject.mappers.ContractMapper;
import com.example.carrentproject.repository.*;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class ContractService {

    ContractMapper contractMapper;
    ContractCRUDRepository contractCRUDRepository;
    CarCRUDRepository carCRUDRepository;
    Contract_PaymentStatusCRUDRepository contractPaymentStatusCRUDRepository;
    CustomerCRUDRepository customerCRUDRepository;
    EmployeeCRUDRepository employeeCRUDRepository;

    public List<ContractDTO> getAllContracts() {

        List<ContractEntity> contractEntities = (List<ContractEntity>) contractCRUDRepository.findAll();
        return contractEntities.stream().map(contractEntity -> contractMapper.toDto(contractEntity)).collect(Collectors.toList());

    }

    public ContractDTO getContract(String contractId) {
        return contractMapper.toDto(contractCRUDRepository.findById(UUID.fromString(contractId))
                .orElseThrow(() -> new NoSuchElementException("Error!")));
    }


    public void createContract(ContractDTO contractDTO) {

        ContractEntity contractEntity = contractMapper.toEntity(contractDTO);
        contractMapper.updateEntity(contractDTO, contractEntity);

        CarEntity carEntity = carCRUDRepository.findByCarId(UUID.fromString(contractDTO.getCarId()))
                .orElse(null);

        Contract_PaymentStatusEntity contractPaymentStatusEntity = contractPaymentStatusCRUDRepository
                .findByContractPaymentStatusId(UUID.fromString(contractDTO.getContractPaymentStatusId()))
                .orElse(null);

        CustomerEntity customerEntity = customerCRUDRepository.findByCustomerId(UUID.fromString(contractDTO.getCustomerId()))
                .orElse(null);

        EmployeeEntity employeeEntity = employeeCRUDRepository.findByEmployeeId(UUID.fromString(contractDTO.getEmployeeId()))
                .orElse(null);


        contractEntity.setCar(carEntity);
        contractEntity.setContractPaymentStatusEntity(contractPaymentStatusEntity);
        contractEntity.setCustomer(customerEntity);
        contractEntity.setEmployee(employeeEntity);

        contractCRUDRepository.save(contractEntity);

    }

    public void deleteContract(String contractId) {
        ContractEntity contractEntity = contractCRUDRepository
                .findByContractId(UUID.fromString(contractId))
                .orElseThrow(() -> new NoSuchElementException("Error!"));

            CarEntity car = contractEntity.getCar();
            car.getContractEntities().remove(contractEntity);
            carCRUDRepository.save(car);

            Contract_PaymentStatusEntity contractPaymentStatusEntity = contractEntity.getContractPaymentStatusEntity();
            contractPaymentStatusEntity.setContract(null);
            contractPaymentStatusCRUDRepository.save(contractPaymentStatusEntity);

            CustomerEntity customer = contractEntity.getCustomer();
            customer.setContractEntity(null);
            customerCRUDRepository.save(customer);

            EmployeeEntity employee = contractEntity.getEmployee();
            employee.setContractEntity(null);
            employeeCRUDRepository.save(employee);

        contractCRUDRepository.deleteByContractId(UUID.fromString(contractId));
    }


    public void updateContract(ContractDTO contractDTO) {

        ContractEntity contractEntity = contractCRUDRepository
                .findByContractId(UUID.fromString(contractDTO.getContractId()))
                .orElseThrow(() -> new NoSuchElementException("Error!"));
        contractMapper.updateEntity(contractDTO, contractEntity);


        CarEntity carEntity = carCRUDRepository.findByCarId(UUID.fromString(contractDTO.getCarId()))
                .orElse(null);

        CustomerEntity customerEntity = customerCRUDRepository.findByCustomerId(UUID.fromString(contractDTO.getCustomerId()))
                .orElse(null);

        EmployeeEntity employeeEntity = employeeCRUDRepository.findByEmployeeId(UUID.fromString(contractDTO.getEmployeeId()))
                .orElse(null);

        contractEntity.setCar(carEntity);
        contractEntity.setCustomer(customerEntity);
        contractEntity.setEmployee(employeeEntity);

        contractCRUDRepository.save(contractEntity);
    }
}
