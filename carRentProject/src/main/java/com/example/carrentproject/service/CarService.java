package com.example.carrentproject.service;

import com.example.carrentproject.DTOs.carDTOs.CarDTO;
import com.example.carrentproject.DTOs.carDTOs.CreateCarDTO;
import com.example.carrentproject.entities.CarEntity;
import com.example.carrentproject.entities.ContractEntity;
import com.example.carrentproject.entities.RatingEntity;
import com.example.carrentproject.mappers.CarMapper;
import com.example.carrentproject.repository.CarCRUDRepository;
import com.example.carrentproject.repository.ContractCRUDRepository;
import com.example.carrentproject.repository.RatingCRUDRepository;
import lombok.AllArgsConstructor;
import org.springframework.expression.ExpressionException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class CarService {

    CarCRUDRepository carCRUDRepository;
    ContractCRUDRepository contractCRUDRepository;
    RatingCRUDRepository ratingCRUDRepository;
    CarMapper carMapper;

    public List<CarDTO> getAllCars() {

        List<CarEntity> carEntities = (List<CarEntity>) carCRUDRepository.findAll();
        return carEntities.stream().map(carEntity -> carMapper.toDto(carEntity)).collect(Collectors.toList());

    }


    public CarDTO getCar(String carId) {

        CarEntity carEntity = carCRUDRepository
                .findByCarId(UUID.fromString(carId))
                .orElseThrow(() -> new NoSuchElementException("Error!"));

        return carMapper.toDto(carEntity);
    }

    public void createCar(CreateCarDTO createCarDTO) throws Exception {
        if(createCarDTO.getContractEntity() == null ){
            throw new Exception("Kein Auto ohne Vertrag!");
        }

        CarEntity carEntity = carMapper.toEntity(createCarDTO);

        List<ContractEntity> contractEntities = new ArrayList<>();
        contractEntities.add(
                contractCRUDRepository.findById(UUID.fromString(createCarDTO.getContractEntity()))
                        .orElseThrow(() -> new ExpressionException("Error!"))
        );

        carEntity.setContractEntities(contractEntities);

        carCRUDRepository.save(carEntity);

    }

    public void deleteCar(String carId) {

        CarEntity carEntity = carCRUDRepository.findByCarId(UUID.fromString(carId))
                .orElseThrow(() -> new NoSuchElementException("Error!"));

        for (ContractEntity contractEntity : carEntity.getContractEntities()) {
            contractEntity.setCar(null);
            contractCRUDRepository.save(contractEntity);
        }
        for (RatingEntity ratingEntity : carEntity.getRatingEntities()) {
            ratingEntity.setRating(null);
            ratingCRUDRepository.save(ratingEntity);
        }

        carCRUDRepository.deleteByCarId(UUID.fromString(carId));

    }

    public void updateCar(CarDTO carDTO) {

        CarEntity carEntity = carCRUDRepository.findByCarId(UUID.fromString(carDTO.getCarId()))
                .orElseThrow(() -> new NoSuchElementException("Error!"));
        carMapper.updateEntity(carDTO, carEntity);

        List<ContractEntity> contractEntities = null;
        List<RatingEntity> ratingEntities = null;

        if (carDTO.getContractEntities() != null && !carDTO.getContractEntities().isEmpty()) {
            contractEntities = carDTO.getContractEntities().stream()
                    .map(
                            s -> contractCRUDRepository.findByContractId(UUID.fromString(s))
                                    .orElseThrow(() -> new NoSuchElementException("Error!"))
                    ).collect(Collectors.toList());
        }
        if (carDTO.getRatingEntities() != null && !carDTO.getRatingEntities().isEmpty()) {
            ratingEntities = carDTO.getRatingEntities().stream()
                    .map(
                            s -> ratingCRUDRepository.findByRatingId(UUID.fromString(s))
                                    .orElseThrow(() -> new NoSuchElementException("Error!"))
                    ).collect(Collectors.toList());
        }

        carEntity.setContractEntities(contractEntities);
        carEntity.setRatingEntities(ratingEntities);

        carCRUDRepository.save(carEntity);
    }
}
