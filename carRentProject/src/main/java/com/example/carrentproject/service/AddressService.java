package com.example.carrentproject.service;

import com.example.carrentproject.DTOs.adressDTOs.AddressDTO;
import com.example.carrentproject.entities.AddressEntity;
import com.example.carrentproject.entities.CustomerEntity;
import com.example.carrentproject.entities.EmployeeEntity;
import com.example.carrentproject.mappers.AddressMapper;
import com.example.carrentproject.repository.AddressCRUDRepository;
import com.example.carrentproject.repository.CustomerCRUDRepository;
import com.example.carrentproject.repository.EmployeeCRUDRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class AddressService {

    AddressMapper addressMapper;
    AddressCRUDRepository addressCRUDRepository;
    CustomerCRUDRepository customerCRUDRepository;
    EmployeeCRUDRepository employeeCRUDRepository;

    public List<AddressDTO> getAllAddresses() {

        List<AddressEntity> addressEntities = (List<AddressEntity>) addressCRUDRepository.findAll();
        return addressEntities.stream().map(addressMapper::toDto).collect(Collectors.toList());

    }

    public AddressDTO getAddress(String addressId) {

        AddressEntity addressEntity = addressCRUDRepository.findByAddressId(UUID.fromString(addressId))
                .orElseThrow(() -> new NoSuchElementException("Error!"));
        return addressMapper.toDto(addressEntity);

    }

    public void createAddress(AddressDTO addressDTO) throws Exception {

        if (addressDTO.getCustomerId() == null || addressDTO.getEmployeeId() == null){
            throw new Exception("userId darf nicht leer sein!");
        }

        AddressEntity addressEntity = addressMapper.toEntity(addressDTO);
        addressMapper.updateEntity(addressDTO, addressEntity);

        CustomerEntity customerEntity = null;
        EmployeeEntity employeeEntity = null;

        if (addressDTO.getCustomerId() != null) {
            customerEntity = customerCRUDRepository.findByCustomerId(UUID.fromString(addressDTO.getCustomerId()))
                    .orElse(null);
        }
        if (addressDTO.getEmployeeId() != null) {
            employeeEntity = employeeCRUDRepository.findByEmployeeId(UUID.fromString(addressDTO.getEmployeeId()))
                    .orElse(null);
        }

        addressEntity.setCustomerEntity(customerEntity);
        addressEntity.setEmployeeEntity(employeeEntity);

        addressCRUDRepository.save(addressEntity);

    }

    public void deleteAddress(String addressId) {

        AddressEntity addressEntity = addressCRUDRepository.findByAddressId(UUID.fromString(addressId))
                .orElseThrow(() -> new NoSuchElementException("Error!"));


        if (addressEntity.getEmployeeEntity() != null) {
            Optional<CustomerEntity> customer = customerCRUDRepository.findByCustomerId(addressEntity.getCustomerEntity().getCustomerId());
            customer.ifPresent(c -> c.setAddress(null));
            customer.ifPresent(customerCRUDRepository::save);
        }
        if (addressEntity.getEmployeeEntity() != null) {
            Optional<EmployeeEntity> employee = employeeCRUDRepository.findByEmployeeId(addressEntity.getEmployeeEntity().getEmployeeId());
            employee.ifPresent(e -> e.setAddress(null));
            employee.ifPresent(employeeCRUDRepository::save);
        }

        addressCRUDRepository.deleteByAddressId(UUID.fromString(addressId));

    }

    public void updateAddress(AddressDTO addressDTO) {

        AddressEntity addressEntity = addressCRUDRepository.findByAddressId(UUID.fromString(addressDTO.getAddressId()))
                .orElseThrow(() -> new NoSuchElementException("Error!"));
        addressMapper.updateEntity(addressDTO, addressEntity);

        EmployeeEntity employeeEntity = employeeCRUDRepository.findByEmployeeId(UUID.fromString(addressDTO.getEmployeeId()))
                .orElse(null);
        CustomerEntity customerEntity = customerCRUDRepository.findByCustomerId(UUID.fromString(addressDTO.getCustomerId()))
                .orElse(null);

        addressEntity.setEmployeeEntity(employeeEntity);
        addressEntity.setCustomerEntity(customerEntity);

        addressCRUDRepository.save(addressEntity);

    }
}
