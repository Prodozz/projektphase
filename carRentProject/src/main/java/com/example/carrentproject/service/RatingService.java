package com.example.carrentproject.service;

import com.example.carrentproject.DTOs.RatingDTOs.RatingDTO;
import com.example.carrentproject.entities.CarEntity;
import com.example.carrentproject.entities.CustomerEntity;
import com.example.carrentproject.entities.RatingEntity;
import com.example.carrentproject.mappers.RatingMapper;
import com.example.carrentproject.repository.CarCRUDRepository;
import com.example.carrentproject.repository.CustomerCRUDRepository;
import com.example.carrentproject.repository.RatingCRUDRepository;
import lombok.AllArgsConstructor;
import org.springframework.expression.ExpressionException;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class RatingService {

    RatingCRUDRepository ratingCRUDRepository;
    CarCRUDRepository carCRUDRepository;
    CustomerCRUDRepository customerCRUDRepository;
    RatingMapper ratingMapper;

    public List<RatingDTO> getAllRatings() {

        List<RatingEntity> ratingEntities = (List<RatingEntity>) ratingCRUDRepository.findAll();
        return ratingEntities.stream().map(ratingMapper::toDto).collect(Collectors.toList());

    }


    public RatingDTO getRating(String ratingId) {

        RatingEntity ratingService = ratingCRUDRepository
                .findByRatingId(UUID.fromString(ratingId))
                .orElseThrow(() -> new NoSuchElementException("Error!"));

        return ratingMapper.toDto(ratingService);

    }

    public void createRating(RatingDTO ratingDTO) {

        RatingEntity ratingEntity = ratingMapper.toEntity(ratingDTO);
        ratingMapper.updateEntity(ratingDTO, ratingEntity);


        CarEntity carEntity = carCRUDRepository
                .findByCarId(UUID.fromString(ratingDTO.getCarId()))
                .orElse(null);
        CustomerEntity customerEntity = customerCRUDRepository
                .findByCustomerId(UUID.fromString(ratingDTO.getCustomerId()))
                .orElse(null);

        ratingEntity.setCustomer(customerEntity);
        ratingEntity.setCar(carEntity);

        ratingCRUDRepository.save(ratingEntity);


    }

    public void deleteRating(String ratingId) throws NoSuchElementException {

        RatingEntity ratingEntity = ratingCRUDRepository
                .findByRatingId(UUID.fromString(ratingId))
                .orElseThrow(() -> new NoSuchElementException("Error!"));


        CarEntity carEntity = ratingEntity.getCar();

        carEntity.setRatingEntities(carEntity.getRatingEntities().stream()
                .filter(rating -> !rating.equals(ratingEntity)).collect(Collectors.toList()));

        carCRUDRepository.save(carEntity);


        CustomerEntity customerEntity = ratingEntity.getCustomer();

        customerEntity.setRatingEntities(customerEntity.getRatingEntities().stream()
                .filter(rating -> !rating.equals(ratingEntity)).collect(Collectors.toList()));

        customerCRUDRepository.save(customerEntity);


        ratingCRUDRepository.delete(ratingEntity);

    }

    public void updateRating(RatingDTO ratingDTO) {


        RatingEntity ratingEntity = ratingCRUDRepository.findByRatingId(UUID.fromString(ratingDTO.getRatingId()))
                .orElseThrow(() -> new NoSuchElementException("Error!"));
        ratingMapper.updateEntity(ratingDTO, ratingEntity);


        CustomerEntity customerEntity = customerCRUDRepository.findByCustomerId(UUID.fromString(ratingDTO.getCustomerId()))
                .orElse(null);

        CarEntity carEntity = carCRUDRepository.findByCarId(UUID.fromString(ratingDTO.getCarId()))
                .orElse(null);

        ratingEntity.setCustomer(customerEntity);
        ratingEntity.setCar(carEntity);

        ratingCRUDRepository.save(ratingEntity);


    }
}
