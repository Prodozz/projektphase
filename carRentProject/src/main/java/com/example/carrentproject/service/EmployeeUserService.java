package com.example.carrentproject.service;

import com.example.carrentproject.DTOs.employeeDTOs.RegisterEmployeeDTO;
import com.example.carrentproject.entities.UserEntity;
import com.example.carrentproject.repository.UserCRUDRepository;
import com.example.carrentproject.security.entities.SecurityUserEntity;
import lombok.AllArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.example.carrentproject.config.UserAuthority.*;

@Service
@AllArgsConstructor
public class EmployeeUserService {

    private EmployeeService employeeService;
    private UserDetailsManager userDetailsManager;
    private PasswordEncoder passwordEncoder;
    private final UserCRUDRepository userCRUDRepository;

    public void createEmployeeUser(RegisterEmployeeDTO registerEmployeeDTO) {

        SecurityUserEntity securityUserEntity = SecurityUserEntity.builder()
                .user(
                        UserEntity.builder()
                                .username(registerEmployeeDTO.getUsername())
                                .password(passwordEncoder.encode(registerEmployeeDTO.getPassword()))
                                .enabled(true)
                                .authority(List.of(EMPLOYEE))
                                .build()
                ).build();

        userDetailsManager.createUser(securityUserEntity);
        employeeService.createEmployee(registerEmployeeDTO, securityUserEntity.getUsername());

    }


    public void deleteEmployeeUser(String employeeId) {
       String username = employeeService.deleteEmployee(employeeId);
        userDetailsManager.deleteUser(username);

    }


//    public void updateEmployeeUser(CustomerUserPropertiesDTO userCredentialsDTO) {
//
//        UserDetails userDetails = User.builder()
//                .username(userCredentialsDTO.getUsername())
//                .password(passwordEncoder.encode(userCredentialsDTO.getPassword()))
//                .roles(userCredentialsDTO.getUserRole())
//                .build();
//
//        userDetailsManager.updateUser(userDetails);
//        employeeService.updateEmployee(userCredentialsDTO);
//
//    }
}
