//package com.example.carrentproject.security.managers;
//
//import com.example.carrentproject.config.UserAuthority;
//import com.example.carrentproject.entities.UserEntity;
//import com.example.carrentproject.repository.UserCRUDRepository;
//import com.example.carrentproject.security.entities.SecurityUserEntity;
//import lombok.AllArgsConstructor;
//import lombok.NoArgsConstructor;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.dao.IncorrectResultSizeDataAccessException;
//import org.springframework.security.access.AccessDeniedException;
//import org.springframework.security.authentication.AuthenticationManager;
//import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
//import org.springframework.security.core.Authentication;
//import org.springframework.security.core.context.SecurityContext;
//import org.springframework.security.core.context.SecurityContextHolder;
//import org.springframework.security.core.context.SecurityContextHolderStrategy;
//import org.springframework.security.core.userdetails.UserCache;
//import org.springframework.security.core.userdetails.UserDetails;
//import org.springframework.security.core.userdetails.UsernameNotFoundException;
//import org.springframework.security.core.userdetails.cache.NullUserCache;
//import org.springframework.security.crypto.password.PasswordEncoder;
//import org.springframework.security.provisioning.UserDetailsManager;
//import org.springframework.stereotype.Service;
//
//import java.util.List;
//import java.util.Optional;
//
//
//@Service
//@AllArgsConstructor
//@NoArgsConstructor
//public class CustomUserDetailsManager implements UserDetailsManager {
//
//    @Autowired
//    private UserCRUDRepository userCRUDRepository;
//    private AuthenticationManager authenticationManager;
//    @Autowired
//    private PasswordEncoder passwordEncoder;
//    private UserCache userCache = new NullUserCache();
//
//    private SecurityContextHolderStrategy securityContextHolderStrategy = SecurityContextHolder
//            .getContextHolderStrategy();
//
//
//    @Override
//    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
//
//        Optional<UserEntity> user = userCRUDRepository.findByUsername(username);
//
//        if (user.isPresent()) {
//            return user.map(SecurityUserEntity::new)
//                    .orElseThrow(() -> new UsernameNotFoundException("Error!"));
//        }
//
//        throw new UsernameNotFoundException("Error!");
//    }
//
//    @Override
//    public void createUser(UserDetails user) {
//
//
//
//        UserEntity userEntity = UserEntity.builder()
//                .username(user.getUsername())
//                .password(passwordEncoder.encode(user.getPassword()))
//                .enabled(user.isEnabled())
//                .authority((List<UserAuthority>) user.getAuthorities())
//                .build();
//
//
//
//        userCRUDRepository.save(userEntity);
//
//    }
//
//    @Override
//    public void updateUser(UserDetails user) {
//
//        Optional<UserEntity> userEntity = userCRUDRepository.findByUsername(user.getUsername());
//
//        if (userEntity.isPresent()) {
//
//            UserEntity entity = UserEntity.builder()
//                    .username(userEntity.get().getUsername())
//                    .password(userEntity.get().getPassword())
//                    .enabled(userEntity.get().isEnabled())
//                    .authority(userEntity.get().getAuthority())
//                    .build();
//
//            userCRUDRepository.save(entity);
//
//        } else {
//            throw new UsernameNotFoundException("Error!");
//
//        }
//
//        userCache.removeUserFromCache(user.getUsername());
//    }
//
//    @Override
//    public void deleteUser(String username) {
//
//        userCRUDRepository.deleteByUsername(username);
//
//    }
//
//
//    @Override
//    public void changePassword(String oldPassword, String newPassword) {
//
//        Authentication currentUser = SecurityContextHolder.getContext().getAuthentication();
//        if (currentUser == null) {
//            throw new AccessDeniedException("Error");
//        }
//
//        String username = currentUser.getName();
//
//        if (authenticationManager != null) {
//
//            authenticationManager
//                    .authenticate(UsernamePasswordAuthenticationToken.unauthenticated(username, oldPassword));
//        } else {
//
//            try {
//                throw new Exception("Error!");
//            } catch (Exception e) {
//                throw new RuntimeException(e);
//            }
//        }
//
//        userCRUDRepository.changePasswordOfUser(newPassword, username);
//
//        Authentication authentication = createNewAuthentication(currentUser, newPassword);
//
//        SecurityContext context = securityContextHolderStrategy.createEmptyContext();
//        context.setAuthentication(authentication);
//
//        securityContextHolderStrategy.setContext(context);
//        userCache.removeUserFromCache(username);
//
//
//    }
//
//    protected Authentication createNewAuthentication(Authentication currentAuth, String newPassword) {
//        UserDetails user = loadUserByUsername(currentAuth.getName());
//
//        UsernamePasswordAuthenticationToken newAuthentication = UsernamePasswordAuthenticationToken.authenticated(user,
//                null, user.getAuthorities());
//
//        newAuthentication.setDetails(currentAuth.getDetails());
//
//
//        return newAuthentication;
//    }
//
//    @Override
//    public boolean userExists(String username) {
//
//        Integer userCount = userCRUDRepository.countByUsername(username);
//
//
//        if (userCount > 1) {
//            throw new IncorrectResultSizeDataAccessException("Error", 1);
//        }
//
//        return userCount == 1;
//    }
//
//}
