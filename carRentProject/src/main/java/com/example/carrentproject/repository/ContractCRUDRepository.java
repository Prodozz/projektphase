package com.example.carrentproject.repository;

import com.example.carrentproject.entities.ContractEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface ContractCRUDRepository extends CrudRepository<ContractEntity, UUID> {



    Optional<ContractEntity> findByContractId (UUID uuid);
    void deleteByContractId (UUID uuid);

}
