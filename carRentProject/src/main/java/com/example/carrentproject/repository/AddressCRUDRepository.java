    package com.example.carrentproject.repository;

    import com.example.carrentproject.entities.AddressEntity;
    import org.springframework.data.repository.CrudRepository;
    import org.springframework.stereotype.Repository;

    import java.util.Optional;
    import java.util.UUID;

    @Repository
    public interface AddressCRUDRepository extends CrudRepository<AddressEntity, UUID> {

        Optional<AddressEntity> findByAddressId(UUID addressId);

        void deleteByAddressId(UUID addressId);


    }
