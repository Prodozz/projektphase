package com.example.carrentproject.repository;

import com.example.carrentproject.DTOs.customerDTOs.RegisterCustomerDTO;
import com.example.carrentproject.entities.CustomerEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
@EnableJpaRepositories
public interface CustomerCRUDRepository extends CrudRepository<CustomerEntity, UUID> {

    @Query("select c from CustomerEntity c where c.customerId = :uuid")
    Optional<CustomerEntity> findByCustomerId(@Param("uuid") UUID uuid);

    @Query("select NEW CustomerEntity(c.customerId, c.lastName, c.firstName, c.birthDate, c.phoneNumber )" +
            " from CustomerEntity c where c.customerId = :customerId")
    Optional<CustomerEntity> getCustomerPropertiesByCustomerId(@Param("customerId") UUID customerId);

    @Query("select NEW CustomerEntity(c.customerId, c.lastName, c.firstName, c.birthDate, c.phoneNumber )" +
            " from CustomerEntity c where c.phoneNumber = :#{#r.phoneNumber} and c.birthDate = :#{#r.birthDate}" +
            " and c.firstName = :#{#r.firstName} and c.lastName = :#{#r.lastName}")
    Optional<CustomerEntity> getCustomerPropertiesByDTO(@Param("r") RegisterCustomerDTO r);

    Optional<CustomerEntity> findByUsername(String username);




}
