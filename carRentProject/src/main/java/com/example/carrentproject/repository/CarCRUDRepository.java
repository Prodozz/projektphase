package com.example.carrentproject.repository;

import com.example.carrentproject.entities.CarEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface CarCRUDRepository extends CrudRepository<CarEntity, UUID> {

    Optional<CarEntity> findByCarId (UUID uuid);
    void deleteByCarId (UUID uuid);

}
