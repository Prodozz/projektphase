package com.example.carrentproject.repository;

import com.example.carrentproject.entities.RatingEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface RatingCRUDRepository extends CrudRepository<RatingEntity, UUID> {

    Optional<RatingEntity> findByRatingId (UUID uuid);
    void deleteByRatingId (UUID uuid);

}
