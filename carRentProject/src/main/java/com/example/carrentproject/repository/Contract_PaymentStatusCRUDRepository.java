package com.example.carrentproject.repository;

import com.example.carrentproject.entities.Contract_PaymentStatusEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface Contract_PaymentStatusCRUDRepository extends CrudRepository<Contract_PaymentStatusEntity, UUID> {

    Optional<Contract_PaymentStatusEntity> findByContractPaymentStatusId (UUID uuid);
    void deleteByContractPaymentStatusId (UUID uuid);

}
