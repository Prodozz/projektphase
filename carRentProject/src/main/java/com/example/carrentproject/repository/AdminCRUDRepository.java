package com.example.carrentproject.repository;

import com.example.carrentproject.entities.AdminEntity;
import org.springframework.expression.spel.ast.OpAnd;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface AdminCRUDRepository {

    Optional<AdminEntity> findByAdminId(UUID adminId);

    void deleteByAdminId(UUID adminId);
}
