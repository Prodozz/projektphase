package com.example.carrentproject.repository;

import com.example.carrentproject.entities.UserEntity;
import jakarta.transaction.Transactional;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface UserCRUDRepository extends CrudRepository<UserEntity, UUID> {


    @Query("select u from UserEntity u where u.username = :username")
    Optional<UserEntity> findByUsername(@Param("username") String username);

    @Transactional
    void deleteByUsername(String username);

    @Modifying
    @Query("update UserEntity set password = :password where username = :username")
    void changePasswordOfUser(@Param("password") String oldPassword,@Param("username") String username);

    Integer countByUsername(String username);



}
