package com.example.carrentproject.repository;

import com.example.carrentproject.DTOs.employeeDTOs.RegisterEmployeeDTO;
import com.example.carrentproject.entities.EmployeeEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import java.util.Optional;
import java.util.UUID;

@Repository
@EnableTransactionManagement
public interface EmployeeCRUDRepository extends CrudRepository<EmployeeEntity, UUID> {

    @Query("select e from EmployeeEntity e where e.employeeId = :uuid")
    Optional<EmployeeEntity> findByEmployeeId(@Param("uuid") UUID uuid);

    @Query("select NEW EmployeeEntity (e.employeeId, e.lastName, e.firstName," +
            " e.birthDate, e.phoneNumber, e.grossSalary, e.employmentType, e.position )" +
            " from EmployeeEntity e " +
            "where e.firstName = :#{#r.firstName} and e.lastName = :#{#r.lastName} " +
            "and e.phoneNumber = :#{#r.phoneNumber} and e.birthDate = :#{#r.birthDate} " +
            "and e.grossSalary = :#{#r.grossSalary} ")
    Optional<EmployeeEntity> getEmployeePropertiesByDTO(@Param("r") RegisterEmployeeDTO r);

    @Query("select NEW EmployeeEntity (e.employeeId, e.lastName, e.firstName," +
            " e.birthDate, e.phoneNumber, e.grossSalary, e.employmentType, e.position )" +
            " from EmployeeEntity e where e.employeeId = :employeeId")
    Optional<EmployeeEntity> getEmployeePropertiesByUserId(@Param("employeeId") UUID employeeId);


}
