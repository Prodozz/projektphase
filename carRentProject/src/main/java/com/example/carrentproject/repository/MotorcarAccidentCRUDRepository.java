package com.example.carrentproject.repository;

import com.example.carrentproject.entities.MotorcarAccidentEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface MotorcarAccidentCRUDRepository extends CrudRepository<MotorcarAccidentEntity, UUID> {

    Optional<MotorcarAccidentEntity> findByMotorcarAccidentId (UUID uuid);
    void deleteByMotorcarAccidentId (UUID uuid);

}
