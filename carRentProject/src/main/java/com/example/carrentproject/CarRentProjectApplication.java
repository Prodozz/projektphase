package com.example.carrentproject;

import lombok.AllArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.UserDetailsManager;

import java.util.List;

import static com.example.carrentproject.config.UserAuthority.ADMIN;
import static org.springframework.security.core.userdetails.User.withUsername;

@SpringBootApplication
@AllArgsConstructor
public class CarRentProjectApplication implements CommandLineRunner {

    private final UserDetailsManager userDetailsManager;
    private final PasswordEncoder passwordEncoder;

    public static void main(String[] args) {
        SpringApplication.run(CarRentProjectApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {

        userDetailsManager.createUser(
                withUsername("Raul")
                        .password(passwordEncoder.encode("12345"))
                        .roles(ADMIN.getAuthority())
                        .build()
        );

        userDetailsManager.createUser(
                withUsername("Mansur")
                        .password(passwordEncoder.encode("12345"))
                        .roles(ADMIN.getAuthority())
                        .build()
        );

        userDetailsManager.createUser(
                withUsername("Stephan")
                        .password(passwordEncoder.encode("12345"))
                        .roles(ADMIN.getAuthority())
                        .build()
        );

    }
}
