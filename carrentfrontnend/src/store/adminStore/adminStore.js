import axios from "axios";
import {defineStore} from "pinia";
import {ref} from "vue";

export const useAdminStore = defineStore('adminStore', () => {
    const admins = ref([])
    const customer = ref([])

    async function getAdmin() {
        const response = await axios.get("http://localhost:8080/api/admin");
        console.log(response.data)
        admins.value = response.data
    }

    async function postAdmin(admin) {
        const axiosResponse = await axios.post("http://localhost:8080/api/admin", admins);
        admins.value.push(axiosResponse.data)
    }

    async function getCustomer() {
        const response = await axios.get("http://localhost:8080/api/customer");
        console.log("response", response.data)
        customer.value = response.data
    }

    return {getAdmin, postAdmin,  getCustomer}
})