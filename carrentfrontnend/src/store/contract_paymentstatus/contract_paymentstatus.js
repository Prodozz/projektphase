import axios from "axios";
import {defineStore} from "pinia";
import {ref} from "vue";

export const usecontract_paymentstatusStore = defineStore('contract_paymentstatusStore', () => {
    const contract_paymentstatus = ref([])

    async function getContract_paymentstatus() {
        const response = await axios.get("http://localhost:8080/api/contract_paymentstatus");
        console.log("response", response.data)
        contract_paymentstatus.value = response.data
    }

    async function postContract_paymentstatus(contract_paymentstatus){
        const axiosResponse = await axios.post("http://localhost:8080/api/contract_paymentstatus", contract_paymentstatus);
        contract_paymentstatus.value.push(axiosResponse.data)
    }

    async function putContract_paymentstatus(contract_paymentstatus) {
        const axiosResponse = await axios.put("http://localhost:8080/api/contract_paymentstatus" + contract_paymentstatus.id, contract_paymentstatus);
        contract_paymentstatus.value.splice(contract_paymentstatus.value.indexOf(contract_paymentstatus.value.find(contract_paymentstatusLoop => contract_paymentstatusLoop.id === contract_paymentstatus.id)), 1, axiosResponse.data)

    }

    async function deleteContract_paymentstatus(contract_paymentstatus) {
        const axiosResponse = await axios.delete("http://localhost:8080/api/contract_paymentstatus" + contract_paymentstatus.id, contract_paymentstatus);
        contract_paymentstatus.value.splice(contract_paymentstatus.value.indexOf(contract_paymentstatus.value.find(contract_paymentstatusLoop => contract_paymentstatusLoop.id === contract_paymentstatus.id)), 1)
    }

    return{getContract_paymentstatus, postContract_paymentstatus, putContract_paymentstatus, deleteContract_paymentstatus}
})