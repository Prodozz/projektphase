import axios from "axios";
import {defineStore} from "pinia";
import {ref} from "vue";

export const useEmployeeStore = defineStore('employeeStore', () => {
    const employee = ref([])


    async function getEmployee() {
        const response = await axios.get("http://localhost:8080/api/employee");
        console.log("response", response.data)
        employee.value = response.data
    }

    async function postEmployee (employee) {
        const axiosResponse = await axios.post("http://localhost:8080/api/employee", employee);
        employee.value.push(axiosResponse.data)
    }

    async function putEmployee (employee) {
        const axiosResponse = await axios.put("http://localhost:8080/api/employee" + employee.id, employee);
        employee.value.splice(employee.value.indexOf(employee.value.find(employeeLoop => employeeLoop.id === employee.id)), 1, axiosResponse.data);

    }

    async function deleteEmployee (employee) {
        const axiosResponse = await axios.delete("http://localhost:8080/api/employee" + employee.id, employee);
        employee.value.splice(employee.value.indexOf(employee.value.find(employeeLoop => employeeLoop.id === employee.id)), 1);
    }

    return {postEmployee, putEmployee, getEmployee, deleteEmployee}
})