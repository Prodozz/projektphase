import axios from "axios";
import {defineStore} from "pinia";
import {ref} from "vue";

export const useAddressStore = defineStore('addressStore', () => {
    const address = ref([])

    async function getAddress() {
        const response = await axios.get("http://localhost:8080/api/address");
        console.log("response", response.data)
        address.value = response.data
    }

    async function postAddress (address) {
        const axiosResponse = await axios.post("http://localhost:8080/api/address", address);
        address.value.push(axiosResponse.data)
    }

    async function putAddress(address) {
        const axiosResponse = await axios.put("http://localhost:8080/api/address" + address.id, address);
        address.value.splice(address.value.indexOf(address.value.find(addressLoop => addressLoop.id === address.id)), 1, axiosResponse.data);
    }

    async function deleteAddress(address) {
        const axiosResponse = await axios.delete("http://localhost:8080/api/address" + address.id, address);
        address.value.splice(address.value.indexOf(address.value.find(addressLoop => addressLoop.id === address.id)), 1);
    }

    return {postAddress, putAddress, getAddress, deleteAddress}
})