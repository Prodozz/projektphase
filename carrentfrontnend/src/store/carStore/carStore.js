import axios from "axios";
import {defineStore} from "pinia";
import {ref} from "vue";
import authHeader from '../oauth2Store/authHeaderStore'

export const useCarStore = defineStore('carStore', () => {
    const allCars = ref([])
    const carById = ref([])
    const carsInList = ref([])

    async function getAllCars() {
        const response = await axios.get("http://localhost:9000/api/car/getAll");
        console.log("allCars", response.data)
        allCars.value = response.data
    }

    async function getCarById(carId) {
        const response = await axios.get("http://localhost:9000/api/car/" + carId); //TODO: getCarById is only for authorized users
        console.log("carById", response.data)
        carById.value = response.data
    }

    async function getCarsInList(carIdList) {
        const response = await axios.get("http://localhost:9000/api/car/carList", carIdList ); //TODO: getCarsInList is only for authorized users
        console.log("carsInList", response.data)
        carById.value = response.data
    }

    async function postCar(car) {
        const axiosResponse = await axios.post("http://localhost:9000/api/car", car);
        allCars.value.push(axiosResponse.data)
    }

    async function putCar(car) {
        const axiosResponse = await axios.put("http://localhost:9000/api/car/update/" + car.carId, car);
        allCars.value.splice(allCars.value.indexOf(allCars.value.find(carLoop => carLoop.id === car.id)), 1, axiosResponse.data)
    }

    async function deleteCar(car) {
        const axiosResponse = await axios.delete("http://localhost:8080/api/car" + car.id);
        allCars.value.splice(allCars.value.indexOf(allCars.value.find(carLoop => carLoop.id === car.id)), 1)
    }

    return {
        getAllCars,
        postCar,
        putCar,
        deleteCar,
        getCarById,
        getCarsInList,
        allCars,
        carById
    }
})