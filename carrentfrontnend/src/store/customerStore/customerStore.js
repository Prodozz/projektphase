import axios from "axios";
import {defineStore} from "pinia";
import {ref} from "vue";

export const useCustomerStore = defineStore("customerStore", () => {
    const onecustomer = ref([])
    const customer = ref([])

    async function getCustomer() {
        const response = await axios.get("http://localhost:8080/api/customer/getAll");
        console.log("response", response.data)
        customer.value = response.data
    }

    async function getOneCustomer() {
        const response = await axios.get("http://localhost:8080/api/customer" + onecustomer.id, onecustomer);
        onecustomer.value.splice(customer.value.indexOf(onecustomer.value.find(onecustomerLoop => onecustomerLoop.id === onecustomer.id)), 1, axiosResponse.data)
    }

    async function postCustomer (customer) {
        try{
        const axiosResponse = await axios.post("http://localhost:8080/api/customer/register", customer);
            console.log(axiosResponse)
            customer.value = axiosResponse.data
        }catch (e) {
            console.log(e)
        }
    }

    async function putStatusCustomer(customer) {
        const axiosResponse = await axios.put("http://localhost:8080/api/customer/setStatus" + customer.id, customer);
        customer.value.splice(customer.value.indexOf(customer.value.find(customerLoop => customerLoop.id === customer.id)), 1, axiosResponse.data)
    }

    async function deleteCustomer (customer) {
        const axiosResponse = await axios.delete("http://localhost:8080/api/customer" + customer.id, customer);
        customer.value.splice(customer.value.indexOf(customer.value.find(customerLoop => customerLoop.id === customer.id)), 1)
    }

    async function loginCustomer (customer) {
            const axiosResponse = await axios.post("http://localhost:8080/api/user/login", customer.value);
            console.log(axiosResponse)
            //customer.value.push(axiosResponse.data);
    }


    return {customer, getCustomer, postCustomer, putStatusCustomer, deleteCustomer, loginCustomer, getOneCustomer}
} )