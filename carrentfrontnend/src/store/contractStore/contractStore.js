import axios, {all} from "axios";
import {defineStore} from "pinia";
import {ref} from "vue";

export const useContractStore = defineStore("contractStore", () => {
    const contract = ref([])
    const allContracts = ref(null)
    const contractById = ref(null)


    async function getAllContracts() {
        try {
            const response = await axios.get("http://localhost:9000/api/contract/getAll")
            console.log("allContracts", response.data)
            allContracts.value = response.data
        } catch (e){
            console.error(e)
        }

    }

    async function getContractById(contractId) {
        try {
            const response = await axios.get("http://localhost:9000/api/contract/" + contractId) // TODO: change this endpoint because its only for authorized users
            console.log("contractById", response.data)
            contractById.value = response.data
        } catch (e){
            console.error(e)
        }
    }

    async function postContract (contract) {
        const axiosResponse = await axios.post("http://localhost:8080/api/contract", contract);
        contract.value.push(axiosResponse.data)
    }

    async function putContract (contract) {
        const axiosResponse = await axios.put("http://localhost:8080/api/contract" + contract.id, contract);
        contract.value.splice(contract.value.indexOf(contract.value.find(contractLoop => contractLoop.id === contract.id)), 1, axiosResponse.data);
    }

    async function deleteContract(contract) {
        const axiosResponse = await axios.delete("http://localhost:8080/api/contract" + contract.id, contract);
        contract.value.splice(contract.value.indexOf(contract.value.find(contractLoop => contractLoop.id === contract.id)),1)
    }

    return {postContract, getAllContracts, putContract, deleteContract, allContracts, getContractById,contractById}
})