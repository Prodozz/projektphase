import axios from "axios";
import {defineStore} from "pinia";
import {ref} from "vue";

export const useMotorCarAccident = defineStore('motorcarAccidentStore', () => {
    const motorcarAccident = ref([])

    async function getMotorcarAccident() {
        const response = await axios.get("http://localhost:8080/api/motorcaraccident");
        console.log("response", response.data)
        motorcarAccident.value = response.data
    }

    async function postMotorcarAccident (motorcaraccident) {
        const axiosResponse = await axios.post("http://localhost:8080/api/motorcaraccident", motorcaraccident);
        motorcaraccident.value.push(axiosResponse.data)
    }

    async function putMotorcarAccident (motorcaraccident) {
        const axiosResponse = await axios.put("http://localhost:8080/api/motorcaraccident" + motorcaraccident.id, motorcaraccident);
        motorcaraccident.value.splice(motorcaraccident.value.indexOf(motorcaraccident.value.find(motorcaraccidentLoop => motorcaraccidentLoop.id === motorcaraccident.id)), 1, axiosResponse.data)

    }

    async function deleteMotorcarAccident (motorcaraccident) {
        const axiosResponse = await axios.put("http://localhost:8080/api/motorcaraccident" + motorcaraccident.id, motorcaraccident);
        motorcaraccident.value.splice(motorcaraccident.value.indexOf(motorcaraccident.value.find(motorcaraccidentLoop => motorcaraccidentLoop.id === motorcaraccident.id)), 1)
    }
})