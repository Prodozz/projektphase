import axios from 'axios'
import {defineStore} from 'pinia'
import {ref} from "vue";
import {sha256} from "js-sha256";
import qs from 'qs';
import {Base64} from "js-base64";
import {generateCodeChallenge, generateCodeVerifier} from "@/store/oauth2Store/pkce";
import {useRoute, useRouter} from "vue-router";
import authHeaderStore from "@/store/oauth2Store/authHeaderStore";

export const useJwtStore = defineStore("JwtStore", () => {

    const isAuthenticated = ref(false)
    const userInfo = ref([])
    const username = ref("")
    const profilePic = ref("")

    const getOpenIdConfig = async () => {

        try {
            const response = await axios.get("http://localhost:8080/.well-known/openid-configuration")
            sessionStorage.setItem("authorization_endpoint", response.data.authorization_endpoint)
            sessionStorage.setItem("token_endpoint", response.data.token_endpoint)
            sessionStorage.setItem("revocation_endpoint", response.data.revocation_endpoint)
            sessionStorage.setItem("issuer", response.data.issuer)
            sessionStorage.setItem("userinfo_endpoint", response.data.userinfo_endpoint)

        } catch (e) {
            console.error(e)
        }

    }

    async function exchangeCodeForToken(code) {

        const auth = Base64.encode("messaging-client:{noop}secret")

        const base = location.origin

        const data = {
            client_id: "messaging-client",
            redirect_uri: base + "/authorized",
            grant_type: "authorization_code",
            code_verifier: sessionStorage.getItem("codeVerifier"),
            code: code
        };

        const options = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Authorization': 'Basic ' + auth
            },
            data: qs.stringify(data),
            url: sessionStorage.getItem("token_endpoint"),
        };

        const response = await axios(options);
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + response.data.access_token
        sessionStorage.setItem("access_token", response.data.access_token)
        sessionStorage.setItem("refresh_token", response.data.refresh_token)
        sessionStorage.setItem("id_token", response.data.id_token)
        sessionStorage.setItem("expires_in", Date.now() + (response.data.expires_in * 1000))

        if (sessionStorage.getItem("access_token") !== null) {
            isAuthenticated.value = true
        }

    }

    async function initiatePKCEFlow() {

        sessionStorage.setItem("codeVerifier", generateCodeVerifier())
        sessionStorage.setItem("codeChallenge", generateCodeChallenge())


        sessionStorage.setItem("authorizeUrl",
            `${sessionStorage.getItem("authorization_endpoint")}?response_type=code&client_id=messaging-client&`
            + `scope=openid%20profile%20address%20email%20phone%20payment&redirect_uri=http://127.0.0.1:5173/authorized&`
            + `code_challenge=${sessionStorage.getItem("codeChallenge")}&code_challenge_method=S256`
        )
        sessionStorage.setItem("savedUrl", window.location.href)
        window.location.href = sessionStorage.getItem("authorizeUrl")
    }

    async function revokeToken() {

        try {
            const auth = Base64.encode("messaging-client:{noop}secret")

            await axios.post(sessionStorage.getItem("revocation_endpoint"), {
                    client_id: "messaging-client",
                    client_secret: "{noop}secret",
                    token: sessionStorage.getItem("access_token"),
                    token_type_hint: 'access_token'
                }, {
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded',
                        'Authorization': 'Basic ' + auth
                    }
                }
            ).then(() => {
                sessionStorage.removeItem("access_token")
                sessionStorage.removeItem("id_token")
                axios.defaults.headers.common['Authorization'] = null
            })

            if (sessionStorage.getItem("refresh_token")) {
                await axios.post(sessionStorage.getItem("revocation_endpoint"), {
                        client_id: "messaging-client",
                        client_secret: "{noop}secret",
                        token: sessionStorage.getItem("refresh_token"),
                        token_type_hint: 'refresh_token'
                    }, {
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded',
                            'Authorization': 'Basic ' + auth
                        }
                    }
                ).then(() => {
                    sessionStorage.removeItem("refresh_token")
                    sessionStorage.removeItem("expires_in")
                    sessionStorage.removeItem("codeVerifier")
                    sessionStorage.removeItem("codeChallenge")
                })
            }
        } catch (e) {
            console.error(e)
        }

        isAuthenticated.value = false
        window.location.href = sessionStorage.getItem("issuer") + "/logout"


    }

    async function refreshAccessToken() {

        const response = await axios.post("http://localhost:8080/oauth2/token", {
            client_id: "messaging-client",
            client_secret: "{noop}secret",
            grant_type: "refresh_token",
            refresh_token: sessionStorage.getItem("refresh_token")
        })

        sessionStorage.setItem("refresh_token", response.data.refresh_token)
        sessionStorage.setItem("expires_in", (new Date().getTime() + response.data.expires_in * 1000).toString())

    }

    async function getUserInfo() {
        try {
            const response = await axios.get(sessionStorage.getItem('userinfo_endpoint'), authHeaderStore.setup().header)
            userInfo.value = response.data
            username.value = response.data.sub
        } catch (error) {
            console.error(error)
        }
    }

    return {
        exchangeCodeForToken,
        initiatePKCEFlow,
        revokeToken,
        getOpenIdConfig,
        getUserInfo,
        refreshAccessToken,
        isAuthenticated,
        userInfo,
        username,
        profilePic
    }
})
