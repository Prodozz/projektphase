export default {
    name: "authHeader",
    setup() {
        const header = {
            headers: {
                "Authorization": "Bearer " + sessionStorage.getItem("access_token")
            }
        }

        return {
            header
        }
    }
}
