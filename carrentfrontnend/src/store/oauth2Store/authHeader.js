export default {
    name: "authHeader",
    setup() {
        const header = {
            headers: {
                "Authorization": "Bearer " + sessionStorage.getItem("access_token")
            }
        }

        // Return the header object so it can be used in the template
        return {
            header
        }
    }
}
