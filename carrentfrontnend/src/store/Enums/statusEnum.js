import {ref} from "vue";
import {defineStore} from "pinia";
import axios from "axios";

export const useStatusEnumStore = defineStore('status', () => {
    const status = ref([])

    // async function getStatusEnum() {
    //     const response = await axios.get("http://localhost:9000/api/enums/status")
    //     status.value = Object.entries(response.data)
    //     console.log(response.data)
    // }

    return {getStatusEnum, status}
});
