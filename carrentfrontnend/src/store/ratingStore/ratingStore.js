import axios from "axios";
import {defineStore} from "pinia";
import {ref} from "vue";

export const useRatingStore = defineStore("ratingStore", () => {
    const rating = ref([])

    async function getRating() {
        const response = await axios.get("http://localhost:9000/api/rating");
        console.log("response", response.data)
        rating.value = response.data
    }

    async function postRating (rating) {
        const axiosResponse = await axios.post("http://localhost:9000/api/rating", rating)
        rating.value.push(axiosResponse.data)
    }

    async function putRating(rating) {
        const axiosResponse = await axios.put("http://localhost:8080/api/rating" + rating.id, rating);
        rating.value.splice(rating.value.indexOf(rating.value.find(ratingLoop => ratingLoop.id === rating.id)), 1, axiosResponse.data);

    }

    async function deleteRating (rating) {
        const axiosResponse = await axios.delete("http://localhost:8080/api/rating" + rating.id, rating);
        rating.value.splice(rating.value.indexOf(rating.value.find(ratingLoop => ratingLoop.id === rating.id)), 1)
    }

    return{getRating, postRating, putRating, deleteRating, rating}
})