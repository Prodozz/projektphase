import {createRouter, createWebHistory} from 'vue-router'
import landingPage from "../views/unregistered/landingPage.vue";
import registrationPage from "../views/unregistered/registrationPage.vue";
import LayoutPage from "../views/LayoutPage.vue";
import loginPage from "../views/unregistered/loginPage.vue";
import changebookingPage from "../views/customer/changebookingPage.vue";
import AdminPanel from "../views/admin/AdminPanel.vue";
import AdminLogin from "../views/admin/AdminLogin.vue";
import pricePage from "../views/unregistered/pricePage.vue";
import miniCarsPage from "../views/unregistered/miniCarsPage.vue";
import standartCars from "../views/unregistered/standartCars.vue";
import luxuryCarPage from "../views/unregistered/luxuryCarPage.vue";
import caprioCarPage from "../views/unregistered/caprioCarPage.vue";
import ContactPage from "../views/unregistered/ContactPage.vue";
import FAQsPage from "../views/unregistered/FAQsPage.vue";
import impressumPage from "../views/unregistered/ImpressumPage.vue";
import datenschutzPage from "../views/unregistered/DatenschutzPage.vue";
import agbsPage from "../views/unregistered/AGBsPage.vue";
import myAccount from "../views/customer/myAccount.vue";
import Authorized from "@/store/oauth2Store/Authorized.vue";
import {useJwtStore} from "@/store/oauth2Store/JwtStore";
import RentACarPage from "@/views/customer/RentACarPage.vue";
import RentACarPagePayment from "@/views/customer/RentACarPagePayment.vue";
import addCarPage from "@/views/admin/addCarPage.vue";
import UserAccount from "@/views/customer/UserAccount.vue"
import CustomerContracts from "@/views/customer/CustomerContractList.vue";
import PersonalData from "@/components/account/PersonalData.vue";
import ChangePassword from "@/components/account/ChangePassword.vue";
import Address from "@/components/account/Address.vue";
import Email from "@/components/account/Email.vue";
import CustomerContract from "@/views/customer/CustomerContract.vue";
import carContract from "@/views/customer/carContract.vue";
import orderEnd from "@/views/customer/orderEnd.vue";

const routes = [
    {
        path: "/",
        component: landingPage,
        name: 'landingpage'
    },
    {
        path: "/registrationPage",
        component: registrationPage,
        name: "registrationPage"
    },
    {
        path: "/loginPage",
        component: loginPage
    },
    {
        path: "/changebookingPage",
        component: changebookingPage,
        name: "changeBookingPage"
    },

    {
        path: "/adminLogin",
        component: AdminLogin,
        name: "adminLogin"
    },
    {
        path: "/price",
        component: pricePage,
        name: "price"
    },
    {
        path: "/admin",
        component: AdminPanel,
        name: "admin"
    },
    {
        path: "/cars",
        component: miniCarsPage,
        name: "cars"
    },
    {
        path: "/standartCars",
        component: standartCars,
        name: "standartCars"
    },
    {
        path: "/luxuryCars",
        component: luxuryCarPage,
        name: "luxuryCars"
    },
    {
        path: "/caprioCars",
        component: caprioCarPage,
        name: "caprioCars"
    },
    {
        path: "/ContactPage",
        component: ContactPage,
        name: "ContactPage"
    },
    {
        path: "/FAQsPage",
        component: FAQsPage,
        name: "FAQsPage"
    },
    {
        path: "/impressumPage",
        component: impressumPage,
        name: "impressumPage"
    },
    {
        path: "/datenschutzPage",
        component: datenschutzPage,
        name: "datenschutzPage"
    },
    {
        path: "/agbsPage",
        component: agbsPage,
        name: "agbsPage"
    },
    {
        path: "/myAccount/contracts/:contractId",
        name: "contract",
        component: CustomerContract
    },
    {
        path: "/myAccount",
        component: myAccount,
        name: "myAccount",
        children: [
            {
                path: "contracts",
                component: CustomerContracts,
                name: "contracts",
            }, {
                path: "personal-data",
                component: PersonalData,
                name: "personal-data"
            }, {
                path: "change-password",
                component: ChangePassword,
                name: "change-password"
            }, {
                path: "address",
                component: Address,
                name: "address"
            }, {
                path: "email",
                component: Email,
                name: "email"
            },
        ]
    },
    {
        path: "/authorized",
        name: 'Authorized',
        component: Authorized
    },
    {
        path: "/RentACarPage",
        component: RentACarPage
    },
    {
        path: "/RentACarPagePayment",
        component: RentACarPagePayment
    },
    {
        path: "/addCar",
        component: addCarPage
    },
    {
        path: "/userAccount",
        component: UserAccount
    },
    {
        path: "/carContract",
        component: carContract
    },
    {
        path: "/order",
        component: orderEnd
    }

]

export const router = createRouter({
    history: createWebHistory(),
    routes,
    scrollBehavior(to, from, savedPosition) {
        if (to.hash) {
            return {
                el: to.hash,
                behavior: 'smooth',
            }
        }
    }
})

router.beforeEach(async (to, from, next) => {

    // Check if the access token is expired
    if (sessionStorage.getItem("access_token") && sessionStorage.getItem("expires_in") <= Date.now()) {
        // Log the user out if the access token is expired
        await useJwtStore().revokeToken()
        return next('/')
    }


    if (to.path === '/authorized') {
        const urlParams = new URLSearchParams(window.location.search)
        const code = urlParams.get('code')

        if (code) {
            try {
                await useJwtStore().exchangeCodeForToken(code)
            } catch (e) {
                console.error(e)
            }
            await router.push(sessionStorage.getItem("savedUrl"))

        } else {
            throw new Error('Authorization code not found in redirect URI')
        }

    }
    next()
})
